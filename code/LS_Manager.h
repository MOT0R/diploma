#ifndef LSMANAGER_H_SENTRY
#define LSMANAGER_H_SENTRY

#include "NewSolution.h"
#include "Instance.h"
#include "Parameters.h"
#include "IterationStatus.h"
#include "LS.h"

class LocalSearchManager
{
    LocalSearch *LS;
    Parameters *param;
public:
    LocalSearchManager(Parameters& parameters, LocalSearch& LSearch) 
    { 
        param = &parameters; 
        LS = &LSearch;
    }
    ~LocalSearchManager() {}

    bool UseLocalSearch(Solution& sol, IterationStatus& status);
};

#endif
