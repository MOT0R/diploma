#ifndef INIT_H_SENTRY
#define INIT_H_SENTRY

#include "Instance.h"
#include "Solution.h"
#include <iostream>
#include <vector>

struct vertex
{
    int take;
    int deliver;
};

class Initializator
{
    struct helicopter;
    const Instance* instance;
    Solution* sol;
    void MakeRoute(helicopter& transport, int day, bool* visited, int size);
    void MakeFirstStep(helicopter& transport, int day, bool* visited, int size);
    int FindTakeVertex(int day, bool* visited, int size) const;
    int FindGreedyVertex(int day) const;
    int GetOverallCanDeliver(int day) const;
    int GetOverallCanTake(int day) const;
public:
    void Init();
    Initializator(Instance& inst, Solution& s);
    ~Initializator();
};


#endif
