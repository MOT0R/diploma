#include "ImpBestLengthInsert.h"
#include "NewSolution.h"
#include <limits>
#include <cassert>

void IBestLengthInsert::EvalBestParam(int day, int& vhc, int& take, int& deliver, size_t& pos, int vtx)
{
    double bestCost = std::numeric_limits<double>::max();
    int planTake = sol->MaxPlanTake(day, vtx);
    assert(planTake >= 0);
    int planDeliver = sol->MaxPlanDeliver(day, vtx);
    assert(planDeliver >= 0);
    for (int v = 0; v < inst->numofvehicles; ++v)
    {
        if (sol->IsInRoute(day, v, vtx))
            continue;
        size_t bestPos = sol->BestLengthPos(day, v, vtx);
        if (bestPos > sol->GetRoutes()[day][v].size())
            continue;
        if (!sol->CanInsert(day, v, vtx, bestPos))
            continue;
        if (sol->InsertCost(day, v, vtx, bestPos) < bestCost)
        {
            bestCost = sol->InsertCost(day, v, vtx, bestPos);
            pos = bestPos;
            day = day;
            vhc = v;
            take = std::min(planTake, sol->MaxPossibleTake(day, vhc, pos));
            assert(take >= 0);
            deliver = std::min(planDeliver, sol->MaxPossibleDeliver(day, vhc, pos));
            assert(deliver >= 0);
        }
    }
}

bool IBestLengthInsert::AddExist(int day, int vtx)
{
    int vhc;
    int pos;
    for (vhc = 0; vhc < inst->numofvehicles; ++vhc)
    {
        if (sol->IsInRoute(day, vhc, vtx))
        {
            route& rt = sol->GetRoutes()[day][vhc];
            for (pos = 0; pos < rt.size(); ++pos)
                if (rt[pos].vertexNum == vtx)
                {
                    int constrTake = sol->MaxPossibleTake(day, vhc, pos);
                    int constrDeliver = sol->MaxPossibleDeliver(day, vhc, pos);
                    int planTake = sol->MaxPlanTake(day, vtx);
                    int planDeliver = sol->MaxPlanDeliver(day, vtx);

                    int take = std::min(constrTake, planTake);
                    int deliver = std::min(constrDeliver, planDeliver);

                    if (take + deliver == 0)
                        continue;

                    sol->GetRoutes()[day][vhc][pos].take += take;
                    sol->GetRoutes()[day][vhc][pos].deliver += deliver;

                    sol->DelUnInserted(vertex(vtx, take, deliver));
                    sol->GetDone()[day][vtx].take += take;
                    sol->GetDone()[day][vtx].deliver += deliver;
                    sol->GetRoutes()[day][vhc].CountDelivered();
                    sol->GetRoutes()[day][vhc].CountTaken();
                    return true;
                }
        }
    }
    return false;
}

int IBestLengthInsert::RandomDay(int vtx)
{
    bool possibleToInsert[inst->numofdays];
    int possibleInsertNum = 0;

    for (int day = 0; day < inst->numofdays; ++day)
    {
        int take = sol->MaxPlanTake(day, vtx);
        int deliver = sol->MaxPlanDeliver(day, vtx);
        if (take + deliver == 0)
            possibleToInsert[day] = false;    
        else
        {
            assert(take + deliver > 0);
            possibleToInsert[day] = true;
            possibleInsertNum++;
        }
    }

    if (possibleInsertNum == 0)
        return -1;

    int randomNum = rand() % possibleInsertNum + 1;
    int day = -1;
    while(randomNum != 0)
    {
        day++;
        if (possibleToInsert[day] == true)
            randomNum--;
    }
    return day;
}

void IBestLengthInsert::RepairSolution(Solution& solution)
{
    sol = &solution;
    const std::vector<vertex>& vec = sol->GetUnInserted();
    while(vec.size())
    {
        int randNum = rand() % vec.size();
        int vtx = vec[randNum].vertexNum;
        int day = RandomDay(vtx);
        if (day == -1)
            continue;
        if (AddExist(day, vtx))
            continue;
        int take = -1;
        int deliver = -1;
        size_t pos = 0;
        int vhc = -1;
        EvalBestParam(day, vhc, take, deliver, pos, vtx);
        sol->AddVertex(day, vhc, vertex(vtx, take, deliver), pos);
    }
}
