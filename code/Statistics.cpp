#include <iostream>
#include <fstream>
#include <vector>
#include "Statistics.h"

Statistics::~Statistics()
{
}

void Statistics::AddEntry(
                        size_t iteration,
                        double newCost,
                        double currentCost,
                        double bestCost)
{
    iterations.push_back(iteration);
    newCosts.push_back(newCost);
    currentCosts.push_back(currentCost);
    bestCosts.push_back(bestCost);
}

void Statistics::GenerateStatsFile()
{
    std::ofstream data;
    data.open("alg_data.csv", std::ios::out | std::ios::trunc);
    data << "iterations;newCost;currentCost;bestCost\n";
    for(size_t i = 0; i < iterations.size(); ++i)
    {
        data << iterations[i] << ';';
        data << newCosts[i] << ';';
        data << currentCosts[i] << ';';
        data << bestCosts[i] << ";\n";
    }
    data.close();
}
