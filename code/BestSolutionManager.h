#ifndef BESTSOLMANAGER_H_SENTRY
#define BESTSOLMANAGER_H_SENTRY

#include <list>

class Solution;
class IterationStatus;

class BestSolutionManager
{
public:
	//! This method evaluate if a solution is a new best solution, and adds it to the
	//! best solution pool in this case.
	//! \param sol the solution to be tested.
	//! \return true if the solution is a new best solution, false otherwise.
	virtual bool IsNewBestSolution(Solution& sol)=0;

	//! Return a pointer to a best solution.
	virtual std::list<Solution*>::iterator begin()=0;

	//! Return a pointer to a best solution.
	virtual std::list<Solution*>::iterator end()=0;

	//! This function take care of reloading the best known
	//! solution, as the current solution, if needed.
	//! \param currSol a pointer to the current solution.
	//! \param status the status of the current iteration.
	//! \return a pointer to the current solution.
	virtual Solution* ReloadBestSolution(Solution* curSol, IterationStatus& status)=0;

    virtual ~BestSolutionManager() {}
};

#endif
