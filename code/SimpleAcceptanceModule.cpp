#include "SimpleAcceptanceModule.h"
#include "NewSolution.h"
#include "IterationStatus.h"
#include "BestSolutionManager.h"
#include "AcceptanceModule.h"

SimpleAcceptanceModule::SimpleAcceptanceModule() {
	// Nothing to be done.

}

SimpleAcceptanceModule::~SimpleAcceptanceModule() {
	// Nothing to be done.
}

bool SimpleAcceptanceModule::TransitionAccepted(BestSolutionManager& bestSolutionManager, Solution& currentSolution, Solution& newSolution, IterationStatus& status)
{
	return newSolution.IsFeasible();
}

