#include <cmath>
#include <algorithm>
#include <iostream>
#include <cassert>
#include <vector>
#include <list>
#include "Solution.h"
#include "Instance.h"
#include "Init.h"
#include "RandomRemove.h"
#include "BestInsert.h"
#include <ctime>

using namespace std;

int main()
{
    srand(time(0));
	Instance A("example_8.txt");
    Solution sol(A);
    sol.RecomputeObj();
    int dayNum = A.numofdays;
    Initializator In(A, sol);
    In.Init();
    for (int day = 0; day < dayNum; day++)
    {
        cout << "day " << day << ":\n";
        sol.PrintSolution(day);
    }
    if (sol.IsFeasible())
        cout << "SOL IS FEASIBLE\n";
    else
        cout << "SOL IS NOT FEASIBLE\n";
    cout << "OBJECTIVE = " << sol.GetObjective() << '\n';
#if 1
    RandomRemove REM(1);
    BestInsert BEST(A);
    int a = 0; 
    while (a)
    {
        REM.DestroySolution(sol, A.numofplatforms);
        sol.PrintSolution();
        BEST.RepairSolution(sol);
        sol.PrintSolution();
        if (sol.IsFeasible())
            cout << "SOL IS FEASIBLE\n";
        else
            cout << "SOL IS NOT FEASIBLE\n";
        cout << "OBJECTIVE = " << sol.GetObjective() << '\n';
        if (sol.GetUnInserted().empty())
            cout << "UNINSERTED IS EMPTY\n";
        else
            cout << "UNINSERTED IS NOT EMPTY\n";
        a--;
        //cin >> a;
    }
#endif
}

