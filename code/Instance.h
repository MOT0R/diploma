#ifndef INSTANCE_H_SENTRY
#define INSTANCE_H_SENTRY

#include <vector>

struct singledata
{
    int reqtake;
    int reqdeliver;
    int cantake;
    int candeliver;
};

struct coordinates
{
    double x;
    double y;
};

struct Instance
{
    int numofplatforms;
    int numofvehicles;
    int vehiclecap;
    int vehiclefuel;
    int numofdays;
    std::vector<std::vector<singledata>> deliverydata;
    std::vector<coordinates> platformcoordinates;
    double **lengths;

    void PrintInstance() const;
    void PrintAmounts() const;
    void PrintCoordinates() const;
    void PrintTake() const;
    void PrintCanTake() const;
    void PrintDeliver() const;
    void PrintCanDeliver() const;
    void PrintLength() const;
    Instance(const char *filename);
    Instance(const Instance& instance);
    ~Instance();
};

#endif
