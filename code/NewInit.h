#ifndef NEWINIT_H_SENTRY
#define NEWINIT_H_SENTRY

#include "Instance.h"
#include "NewSolution.h"
#include <iostream>

class Initializator
{
    Instance *inst;
    Solution *sol;
    bool **inserted;
public:
    Initializator(Instance& in, Solution& s);
    ~Initializator();
    int ChooseNearest(int day, int vehicle, int vtx);
    int CanTake(int day, int vtx);
    int CanDeliver(int day, int vtx);
    void InitOneVehicle(int day, int vehicle);
    void InitOneDay(int day);
    void Init();
};

#endif
