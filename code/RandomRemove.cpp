#include "NewSolution.h"
#include <random>
#include <ctime>
#include "RandomRemove.h"
#include <iostream>

size_t OneRandomRemove::PosInRoute(int day, int vehicle, int vtx)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    for (size_t pos = 0; pos < rt.size(); pos++)
        if (rt[pos].vertexNum == vtx)
            return pos;
    return rt.size();
}

void OneRandomRemove::DestroySolution(Solution& solution)
{
    sol = &solution;
    int randomNum = rand() % (inst->numofplatforms - 1) + 1; 
    for (int day = 0; day < inst->numofdays; day++)
    {
        for (int vehicle = 0; vehicle < inst->numofvehicles; vehicle++)
        {
            if (!sol->IsInRoute(day, vehicle, randomNum))
                continue;
            sol->DelVertex(day, vehicle, PosInRoute(day, vehicle, randomNum));

        }
    }
}
