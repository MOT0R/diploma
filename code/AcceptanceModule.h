#ifndef ACCEPTANCEMODULE_H_SENTRY
#define ACCEPTANCEMODULE_H_SENTRY

#include <iostream>
#include "NewSolution.h"
#include "IterationStatus.h"
#include "BestSolutionManager.h"

/*!
 * \class IAcceptanceModule.
 * \brief This is an interface to define acceptance modules within the ALNS.
 */

class AcceptanceModule
{
public:
	//! Indicate if the new created solution have to be accepted or not
	//! \param bestSolutionManager a reference to the best solution manager.
	//! \param currentSolution current solution.
	//! \param newSolution new solution.
	//! \param status the status of the current alns iteration.
	//! \return true if the transition is accepted, false otherwise.
	virtual bool TransitionAccepted(BestSolutionManager& bestSolutionManager, Solution& currentSolution, Solution& newSolution, IterationStatus& status) = 0;

	//! Some Acceptance modules needs to initialize some variable
	//! only when the solver actualy starts working. In this case
	//! you should override this method.
    virtual ~AcceptanceModule() {};

    virtual void PrintLog(){};
};


#endif
