#include "BestLengthInsert.h"
#include "NewSolution.h"
#include <limits>
#include <cassert>

bool BestLengthInsert::IsUnInserted(int vtx)
{
    const std::vector<vertex>& vec = sol->GetUnInserted();
    for (size_t pos = 0; pos < vec.size(); ++pos)
    {
        if (vec[pos].vertexNum == vtx)
            return true;
    }
    return false;
}

size_t BestLengthInsert::PosInUnInserted(int vtx)
{
    const std::vector<vertex>& vec = sol->GetUnInserted();
    for (size_t pos = 0; pos < vec.size(); ++pos)
    {
        if (vec[pos].vertexNum == vtx)
            return pos;
    }
    return vec.size();
}

size_t BestLengthInsert::VertexPos(int day, int vehicle, int vtx)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        if (rt[pos].vertexNum == vtx)
            return pos;
    }
    return rt.size();
}

void BestLengthInsert::InsertInExisted(int vtx)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            if (!sol->IsInRoute(day, vhc, vtx))
                continue;
            size_t vtxPos = VertexPos(day, vhc, vtx);
            int constrTake = sol->MaxPossibleTake(day, vhc, vtxPos);
            int constrDeliver = sol->MaxPossibleDeliver(day, vhc, vtxPos);
            int planTake = sol->MaxPlanTake(day, vtx);
            int planDeliver = sol->MaxPlanDeliver(day, vtx);
            int take = std::min(constrTake, planTake);
            int deliver = std::min(constrDeliver, planDeliver);
            sol->GetRoutes()[day][vhc][vtxPos].take += take;
            sol->GetRoutes()[day][vhc][vtxPos].deliver += deliver;
            if (take + deliver == 0)
                continue;
            sol->DelUnInserted(vertex(vtx, take, deliver));
            sol->GetDone()[day][vtx].take += take;
            sol->GetDone()[day][vtx].deliver += deliver;
            sol->GetRoutes()[day][vhc].CountDelivered();
            sol->GetRoutes()[day][vhc].CountTaken();
        }
        if (!IsUnInserted(vtx))
            return;
    }
}

void BestLengthInsert::EvalBestParam(int& day, int& vhc, int& take, int& deliver, size_t& pos, int vtx)
{
    double bestCost = std::numeric_limits<double>::max();
    for (int d = 0; d < inst->numofdays; ++d)
    {
        int planTake = sol->MaxPlanTake(d, vtx);
        //std::cout << "PLANTAKE = " << planTake <<'\n';
        assert(planTake >= 0);
        int planDeliver = sol->MaxPlanDeliver(d, vtx);
        //std::cout << "PLANDELIVER = " << planDeliver <<'\n';
        assert(planDeliver >= 0);
        if (planTake + planDeliver == 0)
            continue;
        for (int v = 0; v < inst->numofvehicles; ++v)
        {
            if (sol->IsInRoute(d, v, vtx))
                continue;
            size_t bestPos = sol->BestLengthPos(d, v, vtx);
            if (bestPos > sol->GetRoutes()[d][v].size())
                continue;
            if (!sol->CanInsert(d, v, vtx, bestPos))
                continue;
            if (sol->InsertCost(d, v, vtx, bestPos) < bestCost)
            {
                bestCost = sol->InsertCost(d, v, vtx, bestPos);
                pos = bestPos;
                day = d;
                vhc = v;
                take = std::min(planTake, sol->MaxPossibleTake(d, vhc, pos));
                assert(take >=0);
                deliver = std::min(planDeliver, sol->MaxPossibleDeliver(d, vhc, pos));
                assert(deliver >=0);
            }
        }
    }
}

void BestLengthInsert::InsertInBestPos(int vtx)
{
    while (IsUnInserted(vtx))
    {
        int day = -1;
        int vhc = -1;
        int take = -1;
        int deliver = -1;
        size_t pos = 0;
        EvalBestParam(day, vhc, take, deliver, pos, vtx);
        sol->AddVertex(day, vhc, vertex(vtx, take, deliver), pos);
    }
}

void BestLengthInsert::RepairSolution(Solution& solution)
{
    sol = &solution;
    const std::vector<vertex>& vec = sol->GetUnInserted();
    int vertices[vec.size()];
    size_t size = vec.size();
    for (size_t i = 0; i < size; ++i)
        vertices[i] = vec[i].vertexNum;
    
    for (size_t pos = 0; pos < size; ++pos)
    {
        InsertInExisted(vertices[pos]);
    }
    
    for (size_t pos = 0; pos < size; ++pos)
        InsertInBestPos(vertices[pos]);
}
