#ifndef STATISTICS_H_SENTRY
#define STATISTICS_H_SENTRY

#include <vector>
#include <time.h>

class Statistics
{
public:
    Statistics() {};

    virtual ~Statistics();

    void AddEntry(
            size_t iteration,
            double newCost,
            double currentCost,
            double bestCost);

    void GenerateStatsFile();

    void SetStart() { start = clock(); }
private:
    std::vector<double> rimeStamps;
    std::vector<size_t> iterations;
    std::vector<double> newCosts;
    std::vector<double> currentCosts;
    std::vector<double> bestCosts;
    clock_t start;
};


#endif
