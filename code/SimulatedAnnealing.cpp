#include "SimulatedAnnealing.h"
#include <random>

SimulatedAnnealing::SimulatedAnnealing(CoolingSchedule& cs)
{
    coolingSchedule = &cs;
}

SimulatedAnnealing::~SimulatedAnnealing()
{
    delete coolingSchedule;
}

void SimulatedAnnealing::PrintLog()
{
    coolingSchedule->PrintLog();
}

bool SimulatedAnnealing::TransitionAccepted(BestSolutionManager& bestSolManager,
        Solution& curSol,
        Solution& newSol,
        IterationStatus& status)
{
    double temperature = coolingSchedule->GetCurrentTemperature();
    if (newSol < curSol)
        return true;
    else
    {
        double dif = newSol.GetObjective() - curSol.GetObjective();
        double randomVal = static_cast<double>(rand())/static_cast<double>(RAND_MAX);
        return (exp(-1*dif/temperature)>randomVal);
    }
}

void SimulatedAnnealing::startSignal()
{
}
