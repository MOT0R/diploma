#ifndef INSERTOPERATOR_H_SENTRY
#define INSERTOPERATOR_H_SENTRY

#include "Operator.h"
#include "NewSolution.h"

class RepairOperator : public Operator
{
public:
    RepairOperator() {}
    virtual ~RepairOperator() {}
    virtual void RepairSolution(Solution& solution) = 0;
};

#endif
