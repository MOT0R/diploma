#include "BestDemandInsert.h"
#include <limits>
#include <cassert>

size_t BestDemandInsert::VertexPos(int day, int vehicle, int vtx)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        if (rt[pos].vertexNum == vtx)
            return pos;
    }
    return rt.size();
}

void BestDemandInsert::InsertInExisted(int vtx)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            if (!sol->IsInRoute(day, vhc, vtx))
                continue;
            size_t vtxPos = VertexPos(day, vhc, vtx);
            int constrTake = sol->MaxPossibleTake(day, vhc, vtxPos);
            int constrDeliver = sol->MaxPossibleDeliver(day, vhc, vtxPos);
            int planTake = sol->MaxPlanTake(day, vtx);
            int planDeliver = sol->MaxPlanDeliver(day, vtx);
            int take = std::min(constrTake, planTake);
            int deliver = std::min(constrDeliver, planDeliver);
            sol->GetRoutes()[day][vhc][vtxPos].take += take;
            sol->GetRoutes()[day][vhc][vtxPos].deliver += deliver;
            if (take + deliver == 0)
                continue;
            sol->DelUnInserted(vertex(vtx, take, deliver));
            sol->GetDone()[day][vtx].take += take;
            sol->GetDone()[day][vtx].deliver += deliver;
            sol->GetRoutes()[day][vhc].CountDelivered();
            sol->GetRoutes()[day][vhc].CountTaken();
        }
        if (!IsUnInserted(vtx))
            return;
    }
}

bool BestDemandInsert::IsUnInserted(int vtx)
{
    const std::vector<vertex>& vec = sol->GetUnInserted();
    for (size_t pos = 0; pos < vec.size(); ++pos)
    {
        if (vec[pos].vertexNum == vtx)
            return true;
    }
    return false;
}

void BestDemandInsert::EvalBestParam(int& day, int& vhc, int& take, int& deliver, size_t& pos, int vtx)
{
    int bestAmount = 0;
    for (int d = 0; d < inst->numofdays; ++d)
    {
        int canTake = 0;
        int canDeliver = 0;
        for (int v = 0; v < inst->numofvehicles; ++v)
        {
            if (sol->IsInRoute(d, v, vtx))
                continue;
            size_t bestPos = sol->BestDemandPos(d, v, canTake, canDeliver, vtx);
            if (bestPos > sol->GetRoutes()[d][v].size())
                continue;
            if (!sol->CanInsert(d, v, vtx, bestPos))
                continue;
            if (canTake + canDeliver > bestAmount)
            {
                bestAmount = take + deliver;
                day = d;
                vhc = v;
                take = canTake;
                deliver = canDeliver;
                pos = bestPos;
                assert(take >= 0);
                assert(deliver >= 0);
            }
        }
    }
}

void BestDemandInsert::InsertInBestPos(int vtx)
{
    while (IsUnInserted(vtx))
    {
        int day = -1;
        int vhc = -1;
        int take = -1;
        int deliver = -1;
        size_t pos = 0;
        EvalBestParam(day, vhc, take, deliver, pos, vtx);
        sol->AddVertex(day, vhc, vertex(vtx, take, deliver), pos);
    }
}

void BestDemandInsert::RepairSolution(Solution& solution)
{
    sol = &solution;
    const std::vector<vertex>&vec = sol->GetUnInserted();
    int vertices[vec.size()];
    size_t size = vec.size();
    for (size_t i = 0; i < size; ++i)
        vertices[i] = vec[i].vertexNum;

    for (size_t pos = 0; pos < size; ++pos)
        InsertInExisted(vertices[pos]);

    for (size_t pos = 0; pos < size; ++pos)
        InsertInBestPos(vertices[pos]);
}
