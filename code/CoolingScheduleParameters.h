#ifndef COOLINGSCHEDULEPARAMETERS_H_SENTRY
#define COOLINGSCHEDULEPARAMETERS_H_SENTRY

#include <iostream>
#include "Parameters.h"

class CoolingScheduleParameters
{
public:

    enum CSKind
    {
        LINEAR_IT,
        LINEAR_TIME,
        LINEAR_MIX,
        EXP_IT
    };

    CoolingScheduleParameters(Parameters& param);

    CoolingScheduleParameters();

    CoolingScheduleParameters(CoolingScheduleParameters& p);

    ~CoolingScheduleParameters();

    CSKind kind;

    double expPercentageKept;

    double setupPercentage;

    // The number of temperature recomputations during the optimiztion 
    size_t nbThresholds;

    size_t maxIt;
};

#endif
