#include "RandomRepair.h"
#include "NewSolution.h"
#include <limits>
#include <cassert>
#include "Route.h"

bool RandomRepair::PossibleToInsert(int day, int vehicle, int vtx)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    if (!rt.size())
        return false;
    int cap = inst->vehiclecap;
    if ((rt.Delivered() == cap) && (rt.Taken() == cap))
        return false;
    for (size_t pos = 0; pos < rt.size() + 1; pos++)
    {
        int constrTake = sol->MaxPossibleTake(day, vehicle, pos);
        int constrDeliver = sol->MaxPossibleDeliver(day, vehicle, pos);
        int planTake = sol->MaxPlanTake(day, vtx);
        int planDeliver = sol->MaxPlanDeliver(day, vtx);

        int take = std::min(constrTake, planTake);
        int deliver = std::min(constrDeliver, planDeliver);

        if ((take + deliver > 0) && (sol->CanInsert(day, vehicle, vtx, pos)))
            return true;
    }
    return false;
}

int RandomRepair::RandomVehicle(int day, int vtx)
{
    int vehicleAmount = inst->numofvehicles;
    bool possibleToInsert[vehicleAmount];
    for (int vehicle = 0; vehicle < vehicleAmount; ++vehicle)
    {
        possibleToInsert[vehicle] = PossibleToInsert(day, vehicle, vtx);
    }

    // We can insert in an empty route.
    int possibleInsertNum = 1;
    for (int i = 0; i < vehicleAmount; ++i)
    {
        if (possibleToInsert[i] == true)
            possibleInsertNum++;
    }

    int randomNum = rand() % possibleInsertNum + 1;

    // Return an empty route.
    if (randomNum == possibleInsertNum)
    {
        for (int vhc = 0; vhc < vehicleAmount; ++vhc)
        {
            route& rt = sol->GetRoutes()[day][vhc];
            if (!rt.size())
                return vhc;
        }
    }

    int vehicle = -1;
    while(randomNum != 0)
    {
        vehicle++;
        if (possibleToInsert[vehicle] == true)
            randomNum--;
    }
    return vehicle;
}

int RandomRepair::RandomDay(int vtx)
{
    bool possibleToInsert[inst->numofdays];
    int possibleInsertNum = 0;

    for (int day = 0; day < inst->numofdays; ++day)
    {
        int take = sol->MaxPlanTake(day, vtx);
        int deliver = sol->MaxPlanDeliver(day, vtx);
        if (take + deliver == 0)
            possibleToInsert[day] = false;    
        else
        {
            assert(take + deliver > 0);
            possibleToInsert[day] = true;
            possibleInsertNum++;
        }
    }

    if (possibleInsertNum == 0)
        return -1;

    int randomNum = rand() % possibleInsertNum + 1;
    int day = -1;
    while(randomNum != 0)
    {
        day++;
        if (possibleToInsert[day] == true)
            randomNum--;
    }
    return day;
}

size_t RandomRepair::RandomPosition(int day, int vehicle, int vtx)
{
    int posNum = sol->GetRoutes()[day][vehicle].size() + 1;
    bool possibleInsert[posNum];
    int possibleInsertNum = 0;

    for (size_t pos = 0; pos < posNum; ++pos)
    {
        int planTake = sol->MaxPlanTake(day, vtx);
        int planDeliver = sol->MaxPlanDeliver(day, vtx);
        int constrTake = sol->MaxPossibleTake(day, vehicle, pos);
        int constrDeliver = sol->MaxPossibleDeliver(day, vehicle, pos);

        int take = std::min(planTake, constrTake);
        int deliver = std::min(planDeliver, constrDeliver);

        if (take + deliver == 0)
            possibleInsert[pos] = false;
        else
        {
            possibleInsert[pos] = true;
            possibleInsertNum++;
        }
    }

    if (!possibleInsertNum)
    {
        std::cout << "VEHICLE = " << vehicle << '\n';
        std::cout << "DAY = " << day << '\n';
        std::cout << "VERTEX = " << vtx << '\n';
        sol->Print();
    }

    int randomNum = rand() % possibleInsertNum + 1;
    size_t pos = -1;
    while (randomNum != 0)
    {
        pos++;
        if (possibleInsert[pos] == true)
            randomNum--;
    }

    return pos;
}

void RandomRepair::RepairSolution(Solution& solution)
{
    sol = &solution;
    const std::vector<vertex>& vec = sol->GetUnInserted();
    while (vec.size())
    {
        int randNum = rand() % vec.size();
        int vtx = vec[randNum].vertexNum;
        int day = RandomDay(vtx);
        if (day == -1)
            continue;
        int vehicle = RandomVehicle(day, vtx);
        size_t pos = RandomPosition(day, vehicle, vtx);

        int constrTake = sol->MaxPossibleTake(day, vehicle, pos);
        int constrDeliver = sol->MaxPossibleDeliver(day, vehicle, pos);
        int planTake = sol->MaxPlanTake(day, vtx);
        int planDeliver = sol->MaxPlanDeliver(day, vtx);

        int take = std::min(constrTake, planTake);
        int deliver = std::min(constrDeliver, planDeliver);

        sol->AddVertex(day, vehicle, vertex(vtx, take, deliver), pos);
    }
}
