#include "qRandomRemove.h"

size_t QRandomRemove::RandomPosition(int day, int vehicle)
{
    int randomNum = rand() % sol->GetRoutes()[day][vehicle].size();
    return randomNum;
}

int QRandomRemove::RandomVehicle(int day)
{
    int vehicleNum = 0;
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
    {
        if (sol->GetRoutes()[day][vehicle].size())
            vehicleNum++;
    }
    int randomNum = rand() % vehicleNum + 1;
    int num = -1;
    while (randomNum != 0)
    {
        num++;
        if (sol->GetRoutes()[day][num].size())
            randomNum--;
    }
    return num;
}

int QRandomRemove::NumOfVertices(int day)
{
    int num = 0;
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
    {
        num += sol->GetRoutes()[day][vehicle].size();
    }
    return num;
}


void QRandomRemove::DestroySolution(Solution& solution)
{
    sol = &solution;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        double num = NumOfVertices(day);
        double q = num / 100 * 20;
        int numToRemove = q;
        while(numToRemove != 0)
        {
            int vehicleNum = RandomVehicle(day);
            size_t pos = RandomPosition(day, vehicleNum);
            sol->DelVertex(day, vehicleNum, pos);
            numToRemove--;
        }
    }
}
