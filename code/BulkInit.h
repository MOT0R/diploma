#ifndef BULKINIT_H_SENTRY
#define BULKINIT_H_SENTRY

#include "Instance.h"
#include "NewSolution.h"
#include <iostream>

class BulkInit
{
    Instance *inst;
    Solution *sol;
    bool **inserted;
public:
    BulkInit(Instance& instance, Solution& solution);
    ~BulkInit();
    int ChooseNearest(int day, int vehicle, int vtx);
    void InitOneVehicle(int day, int vehicle);
    void InitOneDay(int day);
    void Init();
};

#endif
