#include "qMinimumQuantRemove.h"

int qMinimumQRemove::NumOfVertices(int day)
{
    int num = 0;
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
    {
        num += sol->GetRoutes()[day][vehicle].size();
    }
    return num;
}

void qMinimumQRemove::WorstPosDemand(int day, int vehicle, size_t& pos, int& demand)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    demand = 2 * inst->vehiclecap;
    pos = 0;
    for (size_t curPos = 0; curPos < rt.size(); ++curPos)
    {
        int curDemand = rt[curPos].deliver + rt[curPos].take;
        if (curDemand < demand)
        {
            demand = curDemand;
            pos = curPos;
        }
    }
}

void qMinimumQRemove::RemoveWorstPos(int day)
{
    int worstVehicle = 0;
    size_t worstPos = 0;
    int worstDemand = 2 * inst->vehiclecap;
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
    {
        route& rt = sol->GetRoutes()[day][vehicle];
        if (!rt.size())
            continue;
        size_t pos = 0;
        int demand = 0;
        WorstPosDemand(day, vehicle, pos, demand);
        if (demand < worstDemand)
        {
            worstDemand = demand;
            worstPos = pos;
            worstVehicle = vehicle;
        }
    }
    sol->DelVertex(day, worstVehicle, worstPos);
}

void qMinimumQRemove::DestroySolution(Solution& solution)
{
    sol = &solution;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        double num = NumOfVertices(day);
        double q = num / 100 * 20;
        int numToRemove = q;
        while(numToRemove != 0)
        {
            RemoveWorstPos(day);
            numToRemove--;
        }
    }
}
