#ifndef QRANDOMREMOVE_H_SENTRY
#define QRANDOMREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class QRandomRemove: public DestroyOperator
{
    Instance *inst;
    Solution *sol;

    size_t RandomPosition(int day, int vehicle);
    int RandomVehicle(int day);
    int NumOfVertices(int day);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~QRandomRemove() {}
    QRandomRemove(Instance& i) : inst(&i) {}
};

#endif
