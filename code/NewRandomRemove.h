#ifndef NEWRANDOMREMOVE_H_SENTRY
#define NEWRANDOMREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class RandomRemove: public DestroyOperator
{
    Instance *inst;
    size_t RandomPosition(int day, int vehicle, Solution& sol);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~RandomRemove() {}
    RandomRemove(Instance& i) : inst(&i) {}
};

#endif
