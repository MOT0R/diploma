#ifndef ALNS_H_SENTRY
#define ALNS_H_SENTRY

#include "Instance.h"
#include "NewSolution.h"
#include "Parameters.h"
#include "OperatorManager.h"
#include "Statistics.h"
#include "AcceptanceModule.h"
#include "LS_Manager.h"
#include <set>

class BestSolutionManager;

class ALNS
{
    Instance* data;
    Solution* curSolution;
    Parameters* param;
    IterationStatus status;
    OperatorManager* opManager;
    Statistics stats;
    BestSolutionManager* bestSolManager;
    AcceptanceModule* acceptanceCriterion;
    LocalSearchManager* lsManager;

    // number of iterations since last weight recomputation.
    size_t nbIterationsWC;

    size_t nbIterations;

    size_t nbIterationsWithoutImprovement;

    size_t nbIterationsWithoutImprovementCurrent;

    size_t nbIterationsWithoutTransition;

    clock_t startingTime;

    double lowerBound;

    std::set<long long> knownKeys;
public:
    ALNS(Instance& instance, 
            Solution& sol,
            Parameters& parameters,
            OperatorManager& opMan,
            BestSolutionManager& solMan,
            AcceptanceModule& acceptanceCrit,
            LocalSearchManager& lsMan
            );
    void solve();
    bool CheckAgainstKnownSolution(Solution& sol);
    void PerformOneIteration();
    bool IsNewBest(Solution* newSol);
    bool TransitionCurrentSolution(Solution* newSol);
    bool IsStoppingCriterionMet();
    void end();
    ~ALNS();
};

#endif
