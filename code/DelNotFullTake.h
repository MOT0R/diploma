#ifndef DELNOTTFULLTAKE_H_SENTRY
#define DELNOTTFULLTAKE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class DelNotFullTake : public DestroyOperator
{
    Solution *sol;
    Instance *inst;
    void DelRoute(int day, int vhc);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~DelNotFullTake() {}
    DelNotFullTake(Instance& i) : sol(0), inst(&i) {}
};


#endif
