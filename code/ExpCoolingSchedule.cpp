#include "ExpCoolingSchedule.h"

ExpCoolingSchedule::ExpCoolingSchedule(Solution& initSol, CoolingScheduleParameters& csParam)
{
    this->maxIt = csParam.maxIt;
    this->currentIt = 0;
    this->currentThreshold = 0;
    this->nbThresholds = csParam.nbThresholds;
    this->decreasingFactor = csParam.expPercentageKept;
    currentTemperature = (csParam.setupPercentage*initSol.GetObjective()) * 3;
}

double ExpCoolingSchedule::GetCurrentTemperature()
{
    currentIt++;
    double percentageIt = ((double)currentIt/maxIt);
    int aimedThreshold = (int)(percentageIt*nbThresholds);
    while(currentThreshold < aimedThreshold)
    {
        currentThreshold++;
        currentTemperature *= decreasingFactor;
    }
    return currentTemperature;
}

void ExpCoolingSchedule::startSignal()
{
}

void ExpCoolingSchedule::PrintLog()
{
    std::cout << "Cur T = " << currentTemperature;
}
