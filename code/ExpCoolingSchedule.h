#ifndef EXPCOOLINGSCHEDULE_H_SENTRY
#define EXPCOOLINGSCHEDULE_H_SENTRY

#include "CoolingSchedule.h"
#include "CoolingScheduleParameters.h"
#include "NewSolution.h"

class ExpCoolingSchedule : public CoolingSchedule
{
    long currentIt;

    long maxIt;

    int currentThreshold;

    int nbThresholds;

    double currentTemperature;

    double decreasingFactor;

public:
    ExpCoolingSchedule(Solution& initSol, CoolingScheduleParameters& csParam);
    virtual ~ExpCoolingSchedule() {};
    virtual double GetCurrentTemperature();
    virtual void startSignal();
    virtual void PrintLog();
};

#endif
