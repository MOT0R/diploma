#include "CoolingScheduleParameters.h"

CoolingScheduleParameters::CoolingScheduleParameters(Parameters& param)
{
    kind = EXP_IT;

    expPercentageKept = 0.99;
    setupPercentage = 0.05;
    nbThresholds = 1000;
    maxIt = param.GetMaxNbIterations();
}

CoolingScheduleParameters::~CoolingScheduleParameters()
{
}

CoolingScheduleParameters::CoolingScheduleParameters(
        CoolingScheduleParameters& p)
{
    kind = p.kind;

    expPercentageKept = p.expPercentageKept;

    setupPercentage = p.setupPercentage;

    nbThresholds = p.nbThresholds;
    
    maxIt = p.maxIt;
}
