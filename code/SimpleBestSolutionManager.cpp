#include "SimpleBestSolutionManager.h"
#include <list>
#include "NewSolution.h"
#include "IterationStatus.h"
#include "Parameters.h"



using namespace std;

SimpleBestSolutionManager::SimpleBestSolutionManager(Parameters& param) {
	parameters = &param;
}

SimpleBestSolutionManager::~SimpleBestSolutionManager() {
	for(list<Solution*>::iterator it = bestSols.begin(); it != bestSols.end(); it++)
	{
		delete (*it);
	}
}

bool SimpleBestSolutionManager::IsNewBestSolution(Solution& sol)
{
	for(list<Solution*>::iterator it = bestSols.begin(); it != bestSols.end(); it++)
	{
		Solution& currentSol = *(*it);
		if(currentSol<sol)
		{
			return false;
		}
		else if(sol<currentSol)
		{
			delete *it;
			it = bestSols.erase(it);
			if(it == bestSols.end())
			{
				break;
			}
		}
		else if(currentSol.GetObjective() == sol.GetObjective())
		{
			return false;
		}
	}
	Solution* copy = sol.Copy();
	bestSols.push_back(copy);
	return true;
}

Solution* SimpleBestSolutionManager::ReloadBestSolution(Solution* curSol, IterationStatus& status)
{
	if(status.GetNbIterationWithoutImprovementSinceLastReload() > 0 &&
	   ((status.GetNbIterationWithoutImprovementSinceLastReload() % parameters->GetReloadFrequency()) == 0))
	{
		status.SetNbIterationWithoutImprovementSinceLastReload(0);
		delete curSol;
		return bestSols.back()->Copy();
	}
	else
	{
		return curSol;
	}
}
