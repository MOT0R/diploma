#ifndef SIMPLEBESTSOLMANAGER_H_SENTRY
#define SIMPLEBESTSOLMANAGER_H_SENTRY

#include "BestSolutionManager.h"
#include <list>

class Solution;
class IterationStatus;
class Parameters;

class SimpleBestSolutionManager: public BestSolutionManager {
public:
	SimpleBestSolutionManager(Parameters& param);

	virtual ~SimpleBestSolutionManager();

	virtual bool IsNewBestSolution(Solution& sol);

	//! Return a pointer to a best solution.
	std::list<Solution*>::iterator begin(){return bestSols.begin();};

	//! Return a pointer to a best solution.
	std::list<Solution*>::iterator end(){return bestSols.end();};

	//! This function take care of reloading the best known
	//! solution, as the current solution, if needed.
	//! \param currSol a pointer to the current solution.
	//! \param status the status of the current iteration.
	//! \return a pointer to the current solution.
	virtual Solution* ReloadBestSolution(Solution* currSol, IterationStatus& status);

	//! Simple getter.
	std::list<Solution*>& GetBestSols(){return bestSols;};
private:
	std::list<Solution*> bestSols;

	Parameters* parameters;

};
#endif
