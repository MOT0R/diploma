#ifndef BESTDEMANDINSERT_H_SENTRY
#define BESTDEMANDINSERT_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include "NewSolution.h"

class BestDemandInsert : public RepairOperator
{
    Solution *sol;
    Instance *inst;
    void EvalBestParam(int& day, int& vhc, int& take, int& deliver, size_t& pos, int vtx);
    void InsertInBestPos(int vtx);
    size_t VertexPos(int day, int vehicle, int vtx);
    bool IsUnInserted(int vtx);
    void InsertInExisted(int vtx);
public:
    BestDemandInsert(Instance& i) : sol(0), inst(&i) {}
    virtual ~BestDemandInsert() {}
    virtual void RepairSolution(Solution& solution);
};

#endif
