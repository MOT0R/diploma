#include "NewRandomRemove.h"

size_t RandomRemove::RandomPosition(int day, int vehicle, Solution& sol)
{
    int randomNum = rand() % sol.GetRoutes()[day][vehicle].size();
    return randomNum;
}

void RandomRemove::DestroySolution(Solution& sol)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhcl = 0; vhcl < inst->numofvehicles; ++vhcl)
        {
            if (!sol.GetRoutes()[day][vhcl].size())
                continue;
            int pos = RandomPosition(day, vhcl, sol);
            sol.DelVertex(day, vhcl, pos);
        }
    }
}
