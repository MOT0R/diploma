#include "LS_Manager.h"

bool LocalSearchManager::UseLocalSearch(Solution& sol, IterationStatus& status)
{
    if (status.GetNewBestSolution() != IterationStatus::TRUE)
    {
        return false;
    }
    else
    {
        return LS->PerformLocalSearch(sol);
    }

}
