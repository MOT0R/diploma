#include "Init.h"
#include <iostream>
#include <stdio.h>

Initializator::~Initializator()
{
}

Initializator::Initializator(Instance& inst, Solution& s)
{
    instance = &inst;
    sol = &s;
}

struct Initializator::helicopter
{
    int vehiclenum;
    int curcap;
    int curfuel;
    int curplatform;

    helicopter(int vehiclenum, int cap = 0, int fuel = 0, int platform = 0) :
        vehiclenum(vehiclenum), curcap(cap), curfuel(fuel), curplatform(platform) {}
};

int Initializator::GetOverallCanDeliver(int day) const
{
    int overalldeliver = 0;
    for (int i = 0; i < instance->numofplatforms; i++)
    {
        overalldeliver += instance->deliverydata[day][i].candeliver;
    }
    return overalldeliver;
}

int Initializator::GetOverallCanTake(int day) const
{
    int overalltake = 0;
    for (int i = 0; i < instance->numofplatforms; i++)
    {
        overalltake += instance->deliverydata[day][i].cantake;
    }
    return overalltake;
}

void Initializator::Init() // greedy initial solution
{
    int fuel = instance->vehiclefuel, numofplatforms = instance->numofplatforms;
    int cap = instance->vehiclecap, vehiclenum = instance->numofvehicles;
    for (int day = 0; day < instance->numofdays; day++)
    {
    int overalltake = GetOverallCanTake(day);
    int overalldeliver = GetOverallCanDeliver(day);
        for (int vehicle = 0; vehicle < vehiclenum; vehicle++)
        {
            helicopter transport(vehicle, cap, fuel);
            bool visited[numofplatforms];
            for (int i = 0; i < numofplatforms; i++)
                visited[i] = false;
            if (!(overalltake + overalldeliver - 
                        sol->GetOverallDoneDeliver(day) - 
                        sol->GetOverallDoneTake(day))) break;
            MakeFirstStep(transport, day, visited, numofplatforms);
            MakeRoute(transport, day, visited, numofplatforms);
        }
    }
}

int Initializator::FindGreedyVertex(int day) const 
{
    int bestplatform = 0;
    int bestvalue = 0;
    for (int platform = 0; platform < instance->numofplatforms; platform++)
    {
        int b = instance->deliverydata[day][platform].candeliver;
        int c = sol->GetDone()[day][platform].deliver;
        if ((b - c) > bestvalue)
        {
            bestvalue = b - c;
            bestplatform = platform;
        }
    }     
    if (!bestvalue)
        return 0;
    return bestplatform; 
}

void Initializator::MakeFirstStep(helicopter& transport, int day, bool* visited, int size) 
{     
    int vertex = FindGreedyVertex(day);
    int candeliver = instance->deliverydata[day][vertex].candeliver;
    int cantake = instance->deliverydata[day][vertex].cantake;
    int donedeliver = sol->GetDone()[day][vertex].deliver;
    int donetake = sol->GetDone()[day][vertex].take;

    int firstdeliver = std::min(candeliver - donedeliver, transport.curcap);
    if (firstdeliver < 0)
        std::cout << "FIRSTDELIVER < 0\n";
    int firsttake = std::min(cantake - donetake, transport.curcap);
    if (firsttake < 0)
        std::cout << "FIRSTTAKE < 0\n";
    sol->AddNode(day, transport.vehiclenum, Vertex(vertex, firsttake, firstdeliver), 1); 
    transport.curcap -= firsttake;
    transport.curfuel -= instance->lengths[0][vertex];
    transport.curplatform = vertex;
    visited[vertex] = true;
}

int Initializator::FindTakeVertex(int day, bool* visited, int size) const
{
    int bestPlatform = 0;
    int bestValue = 0;
    int numofplatforms = instance->numofplatforms;
    for (int platform = 0; (platform < numofplatforms); platform++)
    {
        if (visited[platform]) continue;
        int cantake = instance->deliverydata[day][platform].cantake;
        int donetake = sol->GetDone()[day][platform].take;
        if ((cantake - donetake) > bestValue)
        {
            bestValue = cantake - donetake;
            bestPlatform = platform;
        }
    }
    if (!bestValue) 
        return 0;
    return bestPlatform;
}

void Initializator::MakeRoute(helicopter& transport, int day, bool* visited, int size)
{
    int cap = instance->vehiclecap;
    int pos = 2;
    while(true) // very dangerous!!!!!!!!!!!!!!!!!!!!!!!!!
    {
        int vertex = FindTakeVertex(day, visited, size);
        double lengthToDepo = instance->lengths[vertex][0];
        double lengthToVertex = instance->lengths[transport.curplatform][vertex];
        if  (((transport.curfuel - lengthToDepo - lengthToVertex) < 0) || (!vertex))
        {
            transport.curfuel -= instance->lengths[transport.curplatform][0];
            transport.curplatform = 0;
            return;
        }
        int cantake = instance->deliverydata[day][vertex].cantake;
        int donetake = sol->GetDone()[day][vertex].take;
        int take = std::min(transport.curcap, cantake - donetake); 
        if (!take)
        {
            transport.curfuel -= instance->lengths[transport.curplatform][0];
            transport.curplatform = 0;
            return;
        }
        sol->AddNode(day, transport.vehiclenum, Vertex(vertex, take, 0), pos);
        transport.curcap -= take;
        transport.curfuel -= lengthToVertex;
        transport.curplatform = vertex;
        visited[vertex] = true;
        pos++;
    }
}
