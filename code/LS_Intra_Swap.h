#ifndef INSTRASWAP_H_SETNRY
#define INSTRASWAP_H_SETNRY

#include "LS_Operator.h"

class IntraSwap
{
    const Instance *inst;
    Solution *sol;

    bool CanSwap(const route& rt, size_t firstPos, size_t secondPos, double& cost) const;
    bool IsRouteFeasible(const route& rt) const;
    double Length(const route& rt) const;
    void Swap(int day, int vhc, int firstPos, int secondPos);
public:
    IntraSwap(Instance& instance) : inst(&instance), sol(0) {}
    virtual bool MakeBestNeighbor(Solution& sol, int day);
    virtual ~IntraSwap() {}
};

#endif
