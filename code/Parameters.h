#ifndef PARAMETERS_H_SENTRY
#define PARAMETERS_H_SENTRY

#include <cassert>
#include <iostream>

class Parameters
{
protected:
    size_t maxNbIterations;
    double maxRunningTime;
    size_t maxNbIterationsNoImp;
    bool noise;
    int logFrequency;
    // Indicate after how many iterations should the scores of
    // the operators be recomoputed.
    size_t timeSegmentsIt;

    // Indicate the number of iterations taht should be performed
    // before reinitializetrion of the scores of the operators.
    size_t nbItBeforeReinit;
    int sigma1;
    int sigma2;
    int sigma3;
    double rho;
    double minimumWeight;
    double maximumWeight;
    bool lock; 
    size_t reloadFrequency;
    bool performLocalSearch;
public:
    enum StoppingCriteria
    {
        MAX_IT,
        MAX_RT,
        MAX_IT_NO_IMP,
        ALL
    };
protected:
    StoppingCriteria stopCrit;
public:
    Parameters();
    Parameters(Parameters& p);
    ~Parameters();
    size_t GetReloadFrequency() const { return reloadFrequency; }
    void setReloadFrequency(size_t reloadFrequency)
    {
        assert(!lock);
        this->reloadFrequency = reloadFrequency;
    }

    int GetLogFrequency() const { return logFrequency;}
    void SetLogFrequency(int logFrequency)
    {
        assert(!lock);
        this->logFrequency = logFrequency;
    }
    size_t GetMaxNbIterations() const { return maxNbIterations; }
    size_t GetMaxNbIterationsNoImp() const { return maxNbIterationsNoImp; }
    double GetMaxRunningTime() const { return maxRunningTime; }
    double GetMaximumWeight() const { return maximumWeight; }
    double GetMinimumWeight() const { return minimumWeight; }
    size_t GetNbItBeforeReinit() const { return nbItBeforeReinit; }
    double GetRho() const { return rho; }
    int GetSigma1() const { return sigma1; }
    int GetSigma2() const { return sigma2; }
    int GetSigma3() const { return sigma3; }
    StoppingCriteria GetStopCrit() const { return stopCrit; }
    size_t GetTimeSegmetsIt() const  { return timeSegmentsIt; }
    void SetMaxNbIterations(size_t maxNbIterations)
    {
        assert(!lock);
        this->maxNbIterations = maxNbIterations;
    }

    void SetMaxNbIterationsNoImp(size_t maxNbIterationsNoImp)
    {
        assert(!lock);
        this->maxNbIterationsNoImp = maxNbIterationsNoImp;
    }

    void SetMaxRunningTime(double maxRunningTime)
    {
        assert(!lock);
        this->maxRunningTime = maxRunningTime;
    }

    void SetMaximumWeight(double maximumWeight)
    {
        assert(!lock);
        this->maximumWeight = maximumWeight;
    }

    void SetMinimumWeight(double minimumWeight)
    {
        assert(!lock);
        this->minimumWeight = minimumWeight;
    }
    
    void SetNbItBeforeReinit(size_t nbItBeforeReinit)
    {
        assert(!lock);
        this->nbItBeforeReinit = nbItBeforeReinit;
    }

    void SetRho(double rho)
    {
        assert(!lock);
        this->rho = rho;
    }

    void SetSigma1(int sigma1)
    {
        assert(!lock);
        this->sigma1 = sigma1;
    }

    void SetSigma2(int sigma2)
    {
        assert(!lock);
        this->sigma2 = sigma2;
    }

    void SetSigma3(int sigma3)
    {
        assert(!lock);
        this->sigma3 = sigma3;
    }

    bool GetPerformLocalSearch() const
    {
        return performLocalSearch;
    }

    void SetPerformLocalSearch(bool performLocalSearch)
    {
        this->performLocalSearch = performLocalSearch;
    }

    void SetStopCrit(StoppingCriteria stopCrit)
    {
        assert(!lock);
        this->stopCrit = stopCrit;
    }

    void SetLock()
    {
        lock = true;
    }
    

};

#endif
