#include "WorstDeliverRemove.h"

size_t WorstDeliverRemove::WorstPosition(int day, int vehicle, Solution& sol)
{
    route& rt = sol.GetRoutes()[day][vehicle];
    int minimum = inst->vehiclecap;
    int worstPos = 0;
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        int deliver = rt[pos].deliver;
        if (deliver < minimum)
        {
            minimum = deliver;
            worstPos = pos;
        }
    }
    return worstPos;
}

void WorstDeliverRemove::DestroySolution(Solution& sol)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhcl = 0; vhcl < inst->numofvehicles; ++vhcl)
        {
            if (!sol.GetRoutes()[day][vhcl].size())
                continue;
            int pos = WorstPosition(day, vhcl, sol);
            sol.DelVertex(day, vhcl, pos);
        }
    }
}
