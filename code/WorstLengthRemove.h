#ifndef WORSTLENGTHREMOVE_H_SENTRY
#define WORSTLENGTHREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class WorstLengthRemove : public DestroyOperator
{
    Instance *inst;
    size_t WorstPosition(int day, int vehicle, Solution& sol);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~WorstLengthRemove() {}
    WorstLengthRemove(Instance& i) : inst(&i) {} 
};

#endif
