#ifndef COOLINGSCHEDULEFACTORY_H_SENTRY
#define COOLINGSCHEDULEFACTORY_H_SENTRY

#include "NewSolution.h"
#include "CoolingSchedule.h"
#include "CoolingScheduleParameters.h"

class CoolingScheduleFactory
{
public:
    static CoolingSchedule* MakeCoolingSchedule(Solution& sol, 
            CoolingScheduleParameters& param);
};

#endif
