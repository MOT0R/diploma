#ifndef SOLUTION_H_SENTRY
#define SOLUTION_H_SENTRY

#include <list>
#include <vector>
#include "Instance.h"
#include <iostream>

struct Vertex
{
    int vertex;
    int take;
    int deliver;
    Vertex(int ver = 0, int n_take = 0, int n_deliver = 0) 
        { vertex = ver; take = n_take; deliver = n_deliver; }
};

struct info
{
    int deliver;
    int take;
};

class Solution
{
    double** lengthMatrix;
    Instance* inst;
    int dayNum, vehicleNum, platformNum;
    double obj;
    double maxLen;
    std::vector<Vertex> **routes;
    std::vector<int> unInserted;
    void PrintVector(std::vector<Vertex>& route) const;
    void UpdAfterDelete(std::vector<Vertex>& vec, size_t pos);
    void UpdAfterAdd(std::vector<Vertex>& vec, Vertex vtx);
    std::vector<std::vector<info>> done; // for checking
    double **routeLength;
    bool IsLengthOk();
    bool IsCapOk();
    bool IsReqOk();
    bool IsDemandOk();
public:
    void PrintRouteLength();
    Solution(Instance& in);
    ~Solution();
    double GetObjective();
    int GetDayNum() { return dayNum; }
    int GetVehicleNum() { return vehicleNum; }
    bool IsFeasible();
    bool operator<(Solution& sol);
    void AddNode(int day, int vehicle, Vertex vtx, std::size_t pos);
    void DeleteNode(int day, int vehicle, size_t pos);
    void DeleteNodeVal(int day, int vehicle, int val);
    bool CheckVertex(int day, int vehicle, int vertex);
    void RecomputeObj();
    double InsertCost(int day, int vehicle, int vertex, size_t pos); //unmade
    double RemoveCost(); //unmade
    Solution* Copy();
    std::vector<int>& GetUnInserted();
    void PrintSolution(int day) const;
    void FindBestPosition(int day, int& vehicle, int vertex, int& pos);
    void CompBestInsertCost(int day, int vehicle, int vertex, double& cost, int& pos);

    int GetOverallDoneDeliver(int day) const;
    int GetOverallDoneTake(int day) const;
    const std::vector<std::vector<info>>& GetDone(); 
    void CheckPlanTakeDeliver(int day, int& take, int& deliver, int vertex, const Instance& inst);
    void CheckMaxTakeDeliver(int day, int vehicle, size_t pos, const Instance& inst, int& take, int& deliver);
    bool IsReqComplete(int vertex);
    void AddUnInserted(int vertex);
    void CleanUninserted();
    void PrintSolution();
};

#endif

