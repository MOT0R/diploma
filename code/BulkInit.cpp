#include <limits>
#include "BulkInit.h"
#include "Route.h"

BulkInit::BulkInit(Instance& instance, Solution& solution)
{
    inst = &instance;
    sol = &solution;
    inserted = new bool*[inst->numofdays];
    for (int i = 0; i < inst->numofdays; ++i)
        inserted[i] = new bool[inst->numofplatforms];
    for (int i = 0; i < inst->numofdays; ++i)
        for (int j = 0; j < inst->numofplatforms; ++j)
            inserted[i][j] = false;
}

BulkInit::~BulkInit()
{
    for (int i = 0; i < inst->numofdays; ++i)
        delete [] inserted[i];
    delete [] inserted;
}

int BulkInit::ChooseNearest(int day, int vehicle, int vtx)
{
    double smallestLength = std::numeric_limits<double>::max();
    int platform = -1;
    for (int plt = 1; plt < inst->numofplatforms; plt++)
    {
        if ((!inserted[day][plt]) && (plt != vtx) && 
                !sol->IsInRoute(day, vehicle, plt))
        {
            double length = inst->lengths[vtx][plt];
            if (length < smallestLength)
            {
                smallestLength = length;
                platform = plt;
            }
        }
    }
    return platform;
}

void BulkInit::InitOneVehicle(int day, int vehicle)
{
    bool end = false;
    int curVertex = 0;
    int pos = 0;
    while(!end)
    {
        int newVertex = ChooseNearest(day, vehicle, curVertex);
        if (newVertex == -1)
            return;
        if (!sol->CanInsert(day, vehicle, newVertex, pos))
            return;
        int canTake = sol->MaxPlanTake(day, newVertex);
        int canDeliver = sol->MaxPlanDeliver(day, newVertex);
        if (canTake + canDeliver == 0)
        {
            inserted[day][newVertex] = true;
            continue;
        }
        int possibleTake = sol->MaxPossibleTake(day, vehicle, pos);
        int possibleDeliver = sol->MaxPossibleDeliver(day, vehicle, pos);
        int take = std::min(canTake, possibleTake);
        int deliver = std::min(canDeliver, possibleDeliver);
        if (take + deliver == 0)
            return;
        sol->AddVertex(day, vehicle, vertex(newVertex, take, deliver), pos);
        pos++;
        curVertex = newVertex;
    }
}

void BulkInit::InitOneDay(int day)
{
    int vhc = 0;
    while(sol->GetRoutes()[day][vhc].size())
        vhc++;
    for (int vehicle = vhc; vehicle < inst->numofvehicles; ++vehicle)
        InitOneVehicle(day, vehicle);
}

void BulkInit::Init()
{
    for (int vtx = 0; vtx < inst->numofplatforms; vtx++)
    {
        for (int day = 0; day < inst->numofdays; day++)
        {
            int maxTake = sol->MaxPlanTake(day, vtx);
            int maxDeliver = sol->MaxPlanDeliver(day, vtx);
            if ((maxTake >= inst->vehiclecap) && (maxDeliver >= inst->vehiclecap))
            {
                int vhc = 0;
                while(sol->GetRoutes()[day][vhc].size())
                    vhc++;
                int cap = inst->vehiclecap;
                int times = std::min(maxTake/cap, maxDeliver/cap);
                for (int i = 0; i < times; ++i)
                {
                    sol->AddVertex(day, vhc, vertex(vtx, cap, cap),0);
                    vhc++;
                }
            }
        }
    }
    for (int day = 0; day < inst->numofdays; day++)
        InitOneDay(day);
}

