#include "BestInsert.h"
#include "Solution.h"
#include <limits>
#include <cassert>

BestInsert::BestInsert(Instance& instance)
    : completedDays(instance.numofdays)
{
    this->instance = &instance;
}

BestInsert::~BestInsert()
{
}

void BestInsert::BestDayVehPos(Solution& sol, int vertex, int& day, int& veh, int& pos)
{
    double bestCost = std::numeric_limits<double>::max();
    int bestPos = 0;
    int bestVehicle = 0;
    int bestDay = 0;
    for (int d = 0; d < instance->numofdays; d++)
    {
        if (completedDays[d])
        {
            std::cout << "DAY " << d << " COMPLETED\n";
            continue;
        }
        int curPos = 0;
        int curVehicle = 0;
        sol.FindBestPosition(d, curVehicle, vertex, curPos);
        if (!curPos)
        {
            continue;
        }
        if (sol.InsertCost(d, curVehicle, vertex, curPos) < bestCost)
        {
            bestCost = sol.InsertCost(d, curVehicle, vertex, curPos); 
            bestPos = curPos;
            bestVehicle = curVehicle;
            bestDay = d;
        }
    }
    veh = bestVehicle;
    pos = bestPos;
    day = bestDay;
    assert(pos > 0);
}

void BestInsert::Insert(int vertex, Solution& sol)
{
    int maxtake = 0;
    int maxdeliver = 0;
    int cantake = 0;
    int candeliver = 0;
    int day = 0;
    int veh = 0;
    int pos = 0;
    BestDayVehPos(sol, vertex, day, veh, pos);
    assert(pos > 0);
    sol.CheckMaxTakeDeliver(day, veh, pos, *instance, cantake, candeliver);
    sol.CheckPlanTakeDeliver(day, maxtake, maxdeliver, vertex, *instance);
    assert(maxtake >= 0);
    assert(maxdeliver >= 0);
    if (maxtake + maxdeliver == 0)
    {
        completedDays[day] = true;
        return;
    }
    maxtake = std::min(cantake, maxtake);
    maxdeliver = std::min(candeliver, maxdeliver);
    /*
    std::cout << "\nDAY = " << day <<'\n';
    std::cout << "VEHICLE = " << veh << '\n';
    std::cout << "VERTEX = " << vertex <<'\n';
    std::cout <<"POS = " << pos << '\n';
    std::cout <<"CANTAKE = " << cantake <<'\n';
    std::cout << "CANDELIVER = " << candeliver << '\n';
    */
    assert(cantake + candeliver > 0);
    sol.AddNode(day, veh, Vertex(vertex, maxtake, maxdeliver), pos);
}

void BestInsert::RepairSolution(Solution& sol)
{
    for (size_t i = 0; i < completedDays.size(); i++)
    {
        completedDays[i] = false;
    }
    for(std::vector<int>::iterator it = sol.GetUnInserted().begin(); 
            it != sol.GetUnInserted().end(); ++it)
    {
        while (!sol.IsReqComplete(*it))
        {
            Insert(*it, sol);
        }
    }
    sol.RecomputeObj();
    sol.CleanUninserted();
}
