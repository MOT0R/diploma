template<class T> class route
{
    int sz;
    T* elem;
    int space;
public:
    route() : sz(0), elem(0), space(0) {}
    route(int s, T def = T());
    route(const route<T>&);
    route<T>& operator=(const route<T>&);
    ~route() { delete[] elem; }
    int capacity() const { return space; }
    int size() const { return sz; }
    void reserve(int newalloc);
    void resize(int newsize, T def = T());
    void push_back(const T& d);
    T& operator[](int n) { return elem[n]; }
    const T& operator[](int n) const { return elem[n]; }
};

template<class T>
route<T>::route(const route<T>& a) : sz(a.sz), elem(new T[a.sz])
{
    for (int i = 0; i < a.sz; ++i)
        elem[i] = a.elem[i];
}

template<class T>
route<T>::route(int s, T def) : sz(s), elem(new T[s]), space(s)
{
    for (int i = 0; i < sz; ++i)
        elem[i] = def;
}

template<class T>
route<T>& route<T>::operator=(const route<T>& a)
{
    if (this == &a) return *this;

    if (a.sz <= space)
    {
        for (int i = 0; i < a.sz; ++i)
            elem[i] = a.elem[i];
        sz = a.sz;
        return *this;
    }

    T* p = new T[a.sz];
    for (int i = 0; i < a.sz; ++i)
        p[i] = a.elem[i];

    delete[] elem;
    space = sz = a.sz;
    elem = p;
    return *this;
}

template<class T>
void route<T>::push_back(const T& d)
{
    if (space == 0) reserve(8);
    else if (sz == space)
        reserve (2*space);
    elem[sz] = d;
    ++sz;
}

template<class T>
void route<T>::reserve(int newalloc)
{
    if (newalloc <= space) return;
    T* p = new T[newalloc];
    for (int i = 0; i < sz; ++i)
        p[i] = elem[i];

    delete[] elem;
    elem = p;
    space = newalloc;
}

template<class T>
void route<T>::resize(int newsize, T def = T())
{
    reserve(newsize);
    for (int i = sz; i < newsize; ++i)
        elem[i] = def;
    sz = newsize;
}

