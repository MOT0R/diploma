#ifndef LENGTHINSERT_H_SENTRY
#define LENGTHINSERT_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include "NewSolution.h"

class LengthInsert : public RepairOperator
{
    struct iBestParam;
    Solution *sol;
    Instance *inst;
    void InsertInExisted(int vtx);
    bool IsUnInserted(int vtx);
    void EvalBestParam(int& day, int& vhc, size_t& pos, double& cost, int vtx);
    size_t VertexPos(int day, int vehicle, int vtx);
public:
    LengthInsert(Instance& instance) : inst(&instance) {}
    virtual ~LengthInsert() {}
    virtual void RepairSolution(Solution& solution);
};

#endif
