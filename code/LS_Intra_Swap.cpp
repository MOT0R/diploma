#include "LS_Intra_Swap.h"
#include <limits>

double IntraSwap::Length(const route& rt) const
{
    if (!rt.size())
        return 0.0;
    double result = 0.0;
    result += inst->lengths[0][rt[0].vertexNum];
    for (size_t pos = 0; pos < rt.size() - 1; pos++)
    {
        result += inst->lengths[rt[pos].vertexNum][rt[pos + 1].vertexNum];
    }
    result += inst->lengths[0][rt[rt.size() - 1].vertexNum];
    return result;
}

bool IntraSwap::IsRouteFeasible(const route& rt) const
{
    int onBoard = rt.Taken();
    if (onBoard > inst->vehiclecap)
        return false;
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        onBoard = rt.OnBoard(pos);
        if (onBoard > inst->vehiclecap)
            return false;
    }
    return true;
}

bool IntraSwap::CanSwap(const route& rt, size_t firstPos, size_t secondPos, double& cost) const
{
    route newRoute;
    newRoute = rt;
    vertex firstVertex = newRoute[firstPos];
    vertex secondVertex = newRoute[secondPos];
    if (firstPos < secondPos)
    {
        newRoute.DelVertex(secondPos);
        newRoute.DelVertex(firstPos);
        newRoute.AddVertex(secondVertex, firstPos);
        newRoute.AddVertex(firstVertex, secondPos);
    }
    else
    {
        newRoute.DelVertex(firstPos);
        newRoute.DelVertex(secondPos);
        newRoute.AddVertex(firstVertex, secondPos);
        newRoute.AddVertex(secondVertex, firstPos);
    }

    newRoute.CountDelivered();
    newRoute.CountTaken();

    cost = Length(newRoute) - Length(rt);
    return IsRouteFeasible(newRoute);
}

bool IntraSwap::MakeBestNeighbor(Solution& solution, int day)
{
    sol = &solution;
    double bestCost = std::numeric_limits<double>::max();
    int bestVhc = 0;
    int bestFirst = 0;
    int bestSecond = 0;
    bool swapExist = false;
    for (int vhc = 0; vhc < inst->numofvehicles; vhc++)
    {
        if (sol->GetRoutes()[day][vhc].size() <= 1)
            continue;
        route& rt = sol->GetRoutes()[day][vhc];
        for (size_t firstPos = 0; firstPos < rt.size() - 1; ++firstPos)
        {
            for (size_t secondPos = firstPos + 1; secondPos < rt.size(); ++secondPos)
            {
                double curCost = 0.0;
                if (CanSwap(rt, firstPos, secondPos, curCost))
                {
                    swapExist = true;
                    if (curCost < bestCost)
                    {
                        bestCost = curCost;
                        bestVhc = vhc;
                        bestFirst = firstPos;
                        bestSecond = secondPos;
                    }
                }
            }
        }
    }
    if ((swapExist) && (bestCost < 0))
    {
        Swap(day, bestVhc, bestFirst, bestSecond);
        return true;
    }
    return false;
}

void IntraSwap::Swap(int day, int vhc, int firstPos, int secondPos)
{
    vertex firstVertex = sol->GetRoutes()[day][vhc][firstPos];
    vertex secondVertex = sol->GetRoutes()[day][vhc][secondPos];
    if (firstPos < secondPos)
    {
        sol->DelVertex(day, vhc, secondPos);
        sol->DelVertex(day, vhc, firstPos);
        sol->AddVertex(day, vhc, secondVertex, firstPos);
        sol->AddVertex(day, vhc, firstVertex, secondPos);
    }
    else
    {
        sol->DelVertex(day, vhc, firstPos);
        sol->DelVertex(day, vhc, secondPos);
        sol->AddVertex(day, vhc, firstVertex, secondPos);
        sol->AddVertex(day, vhc, secondVertex, firstPos);
    }
}
