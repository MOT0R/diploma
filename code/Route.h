#ifndef ROUTE_H_SENTRY
#define ROUTE_H_SENTRY

#include <vector>
#include <iostream>

struct vertex
{
    int vertexNum;
    int take;
    int deliver;

    vertex(int vnum, int take, int deliver)
    {
        this->vertexNum = vnum;
        this->take = take;
        this->deliver = deliver;
    }
};

class route
{
    int takenFromDepot;
    int deliveredToDepot;
    std::vector<vertex> vec;
    route(const route&);
public:
    route(); 
    size_t size() const { return vec.size(); }
    void AddVertex(const vertex&, size_t pos);
    void DelVertex(size_t pos);
    void CountTaken();
    void CountDelivered();
    int OnBoard(size_t) const;
    int Taken() const { return takenFromDepot; }
    int Delivered() const { return deliveredToDepot; }
    vertex& operator[](size_t i) { return vec[i]; }
    const vertex& operator[](size_t i) const { return vec[i]; }
    const route& operator=(const route&);
    void Print() const;
};

#endif
