#ifndef NEWSOLUTION_H_SENTRY
#define NEWSOLUTION_H_SENTRY

#include "Instance.h"
#include "Route.h"

struct info
{
    int deliver;
    int take;
    info(int d = 0, int t = 0) : deliver(d), take(t) {}
};

class Solution
{
    double obj;
    Instance* inst;
    route **routes;
    std::vector<vertex> unInserted;
    double **routeLength;
    info **done;
    bool IsLengthFeasible();
    bool IsCapFeasible();
    bool IsDemandComplete();
    void InitUnInserted();
    double EvalRouteLength(int day, int vhc);
public:
    Solution(const Solution& solution);
    const Solution& operator=(const Solution& solution);
    Solution(Instance& i);
    ~Solution();
    double GetObjective() const { return obj; }
    info** GetDone() const { return done; }
    route** GetRoutes() const { return routes; }
    const std::vector<vertex>& GetUnInserted() const { return unInserted; }
    void RecomputeLength();
    void RecomputeObj();
    void AddUnInserted(vertex);
    void DelUnInserted(vertex);
    bool IsInRoute(int day, int vehicle, int vtx);
    double InsertCost(int day, int vehicle, int vtx, size_t pos);
    double DeleteCost(int day, int vehicle, size_t pos);
    void AddVertex(int day, int vehicle, vertex vtx, size_t pos);
    void DelVertex(int day, int vehicle, size_t pos);
    int MaxPossibleDeliver(int day, int vehicle, size_t pos);
    int MaxPossibleTake(int day, int vehicle, size_t pos);
    int MaxPlanTake(int day, int vtx);
    int MaxPlanDeliver(int day, int vtx);
    size_t BestLengthPos(int day, int vehicle, int vtx);
    size_t BestDemandPos(int day, int vehicle, int& take, int& deliver, int vtx);
    bool CanInsert(int day, int vehicle, int vtx, size_t pos);
    void Print() const;
    bool IsFeasible();
    Solution* Copy();
    long long GetHash();

    bool operator<(Solution& sol);
};

#endif
