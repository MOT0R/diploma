#ifndef BESTLENGTHINSERT_H_SENTRY
#define BESTLENGTHINSERT_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include "NewSolution.h"

class BestLengthInsert : public RepairOperator
{
    Solution *sol;
    Instance *inst;
    void InsertInExisted(int vtx);
    bool IsUnInserted(int vtx);
    size_t PosInUnInserted(int vtx);
    size_t VertexPos(int day, int vehicle, int vtx);
    void EvalBestParam(int& day, int& vhc, int& take, int& deliver, size_t& pos, int vtx);
    void InsertInBestPos(int vtx);
public:
    BestLengthInsert(Instance& i) : sol(0), inst(&i) {}
    virtual ~BestLengthInsert() {}
    virtual void RepairSolution(Solution& solution);
};

#endif
