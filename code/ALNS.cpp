#include "NewSolution.h"
#include "ALNS.h"
#include "BestSolutionManager.h"
#include "LS.h"
#include <ctime>

ALNS::ALNS(Instance& instance,
        Solution& sol,
        Parameters& parameters,
        OperatorManager& opMan,
        BestSolutionManager& solMan,
        AcceptanceModule& acceptanceCrit,
        LocalSearchManager& lsMan)
{
    curSolution = sol.Copy();
    param = &parameters;
    lowerBound = 0;
    nbIterationsWC = 0;
    nbIterations = 0;
    nbIterationsWithoutImprovement = 0;
    opManager = &opMan;
    data = &instance;

    acceptanceCriterion = &acceptanceCrit;

    opManager->SetStatistics(&stats);

    nbIterationsWithoutImprovementCurrent = 0;

    nbIterationsWithoutTransition = 0;

    bestSolManager = &solMan;

    bestSolManager->IsNewBestSolution(sol);

    lsManager = &lsMan;
}

void ALNS::solve()
{
    startingTime = clock();
    param->SetLock();
    stats.SetStart();
    stats.AddEntry(nbIterations, curSolution->GetObjective(), 
            curSolution->GetObjective(), curSolution->GetObjective());
    while(!IsStoppingCriterionMet())
    {
        PerformOneIteration();
    }
    stats.GenerateStatsFile();
}

ALNS::~ALNS()
{
    delete curSolution;
}

void ALNS::PerformOneIteration()
{
    status.PartialReinit();

    RepairOperator& repair = opManager->SelectRepairOperator();
    DestroyOperator& destroy = opManager->SelectDestroyOperator();

    Solution* newSolution = curSolution->Copy();

    if (nbIterations % param->GetLogFrequency() == 0)
    {
        std::cout << "[ALNS] it. " << nbIterations;
        std::cout << " best sol: " << (*(bestSolManager->begin()))->GetObjective();
        std::cout << " cur sol: " << curSolution->GetObjective();
        acceptanceCriterion->PrintLog();
        std::cout << " known: " << knownKeys.size();
        std::cout << '\n';
    }

    destroy.DestroySolution(*newSolution);

    status.SetAlreadyDestroyed(IterationStatus::TRUE);
    status.SetAlreadyRepaired(IterationStatus::FALSE);

    repair.RepairSolution(*newSolution);
    status.SetAlreadyRepaired(IterationStatus::TRUE);

    nbIterations++;
    status.SetIterationId(nbIterations);
    nbIterationsWC++;

    double newCost = newSolution->GetObjective();
    IsNewBest(newSolution);
    CheckAgainstKnownSolution(*newSolution);
    bool betterThanCurrent = (*newSolution) < (*curSolution);
    if (betterThanCurrent)
    {
        nbIterationsWithoutImprovementCurrent = 0;
        status.SetImproveCurrentSolution(IterationStatus::TRUE);
    }
    else
    {
        nbIterationsWithoutImprovementCurrent++;
        status.SetImproveCurrentSolution(IterationStatus::FALSE);
    }
    status.SetNbIterationWithoutImprovementCurrent(nbIterationsWithoutImprovementCurrent);

    if (param->GetPerformLocalSearch() && lsManager->UseLocalSearch(*newSolution, status))
    {
        bestSolManager->IsNewBestSolution(*newSolution);
    }

    bool transitionAccepted = TransitionCurrentSolution(newSolution);

    if (transitionAccepted)
    {
        status.SetAcceptedAsCurrentSolution(IterationStatus::TRUE);
        nbIterationsWithoutTransition = 0;
    }
    else
    {
        status.SetAcceptedAsCurrentSolution(IterationStatus::FALSE);
        nbIterationsWithoutTransition++;
    }
    status.SetNbIterationWithoutTransition(nbIterationsWithoutTransition);

    opManager->UpdateScores(destroy, repair, status);

    if ((nbIterations % param->GetLogFrequency() == 0) || (nbIterations == 0))
        stats.AddEntry(nbIterations, newCost, curSolution->GetObjective(),
                (*(bestSolManager->begin()))->GetObjective());

    if (nbIterationsWC % param->GetTimeSegmetsIt() == 0)
    {
        opManager->RecomputeWeights();
        nbIterationsWC = 0;
    }

    curSolution = bestSolManager->ReloadBestSolution(curSolution, status);

    delete newSolution;
}

bool ALNS::CheckAgainstKnownSolution(Solution& sol)
{
    bool notKnownSolution = false;
    long long keySol = sol.GetHash();

    if (knownKeys.find(keySol) == knownKeys.end())
    {
        notKnownSolution = true;
        knownKeys.insert(keySol);
    }

    if(!notKnownSolution)
    {
        status.SetAlreadyKnownSolution(IterationStatus::TRUE);
    }
    else 
    {
        status.SetAlreadyKnownSolution(IterationStatus::FALSE);
    }

    return notKnownSolution;
}

bool ALNS::IsNewBest(Solution* newSol)
{
    if (bestSolManager->IsNewBestSolution(*newSol))
    {
        status.SetNewBestSolution(IterationStatus::TRUE);
        nbIterationsWithoutImprovement = 0;
        status.SetNbIterationWithoutImprovement(nbIterationsWithoutImprovement);
        status.SetNbIterationWithoutImprovementSinceLastReload(0);
        return true;
    }
    else
    {
        status.SetNewBestSolution(IterationStatus::FALSE);
        nbIterationsWithoutImprovement++;
        status.SetNbIterationWithoutImprovement(nbIterationsWithoutImprovement);
        status.SetNbIterationWithoutImprovementSinceLastReload(status.GetNbIterationWithoutImprovementSinceLastReload()+1);
        return false;
    }
}

bool ALNS::TransitionCurrentSolution(Solution* newSol)
{
    if (acceptanceCriterion->TransitionAccepted(*bestSolManager, *curSolution, *newSol, status))
    {
        delete curSolution;
        curSolution = newSol->Copy();
        return true;
    }
    else 
    {
        return false;
    }
}

bool ALNS::IsStoppingCriterionMet() // TODO: add new criteria
{
    if (param->GetStopCrit() == Parameters::MAX_IT)
    {
        if (nbIterations >= param->GetMaxNbIterations())
        {
            (*(bestSolManager->begin()))->Print();
            if ((*(bestSolManager->begin()))->IsFeasible())
                std::cout << "FEASIBLE\n";
            else
                std::cout << "INFEASIBLE\n";
            std::cout << "OBJ: " << (*(bestSolManager->begin()))->GetObjective();
            std::cout << '\n';
            /*
            Solution newSol(*(*(bestSolManager->begin())));
            newSol.Print();
            LocalSearch LS(*data);
            LS.PerformLocalSearch(newSol);
            newSol.Print();
            if (newSol.IsFeasible())
                std::cout << "FEASIBLE\n";
            else
                std::cout << "INFEASIBLE\n";
            std::cout << "OBJ: " << newSol.GetObjective();
            std::cout << '\n';
            */
        }
        return (nbIterations >= param->GetMaxNbIterations());
    }
}

void ALNS::end()
{
    opManager->end();
    delete opManager;
    delete acceptanceCriterion;
    delete bestSolManager;
}
