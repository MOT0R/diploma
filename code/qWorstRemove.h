#ifndef QWORSTREMOVE_H_SENTRY
#define QWORSTREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class qWorstRemove : public DestroyOperator
{
    Instance *inst;
    Solution *sol;

    size_t WorstPosition(int day, int vehicle);
    int NumOfVertices(int day);
    void WorstPosCost(int day, int vehicle, size_t& pos, double& cost);
    void RemoveWorstPos();
public:
    virtual void DestroySolution(Solution& solution);
    virtual ~qWorstRemove() {}
    qWorstRemove(Instance& instance) : inst(&instance) {}
};

#endif
