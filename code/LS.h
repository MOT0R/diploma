#ifndef LS_H_SENTRY
#define LS_H_SENTRY

#include "NewSolution.h"
#include "Instance.h"


class LocalSearch
{
    Instance *inst;
    Solution *sol;
    enum operators { INTERSWAP, INTRASWAP, INTERREASSIGNMENT, NONE };
public:
    bool PerformLocalSearch(Solution& sol);
    bool PerformOneIteration();
    LocalSearch(Instance& instance) : inst(&instance), sol(0) {} 
};

#endif
