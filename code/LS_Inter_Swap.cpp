#include "LS_Inter_Swap.h"
#include <limits>

double InterSwap::Length(const route& rt) const
{
    if (!rt.size())
        return 0.0;
    double result = 0.0;
    result += inst->lengths[0][rt[0].vertexNum];
    for (size_t pos = 0; pos < rt.size() - 1; pos++)
    {
        result += inst->lengths[rt[pos].vertexNum][rt[pos + 1].vertexNum];
    }
    result += inst->lengths[0][rt[rt.size() - 1].vertexNum];
    return result;
}

bool InterSwap::IsRouteFeasible(const route& rt) const
{
    int onBoard = rt.Taken();
    if (onBoard > inst->vehiclecap)
        return false;
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        onBoard = rt.OnBoard(pos);
        if (onBoard > inst->vehiclecap)
            return false;
    }
    return true;
}

bool InterSwap::CanSwap(const route& firstRt, const route& secondRt, size_t firstPos, size_t secondPos, double& cost) const
{
    route newFirst;
    route newSecond;
    newFirst = firstRt;
    newSecond = secondRt;
    
    vertex firstVertex = newFirst[firstPos];
    vertex secondVertex = newSecond[secondPos];

    newFirst.DelVertex(firstPos);
    newSecond.DelVertex(secondPos);
    newFirst.AddVertex(secondVertex, firstPos);
    newSecond.AddVertex(firstVertex, secondPos);

    newFirst.CountTaken();
    newFirst.CountDelivered();
    newSecond.CountTaken();
    newSecond.CountDelivered();

    if ((!IsRouteFeasible(newFirst)) || (!IsRouteFeasible(newSecond)))
        return false;

    cost = Length(newFirst) + Length(newSecond) - Length(firstRt) - Length(secondRt);
    return true;
}

bool InterSwap::MakeBestNeighbor(Solution& solution, int day)
{
    sol = &solution;
    double bestCost = std::numeric_limits<double>::max();
    int bestFirstVhc = 0;
    int bestSecondVhc = 0;
    size_t bestFirstPos = 0;
    size_t bestSecondPos = 0;
    bool swapExist = false;

    for (int firstVhc = 0; firstVhc < inst->numofvehicles - 1; firstVhc++)
    {
        if (!sol->GetRoutes()[day][firstVhc].size())
            continue;
        for (int secondVhc = firstVhc + 1; secondVhc < inst->numofvehicles; secondVhc++)
        {
            if (!sol->GetRoutes()[day][secondVhc].size())
                continue;
            route& firstRt = sol->GetRoutes()[day][firstVhc];
            route& secondRt = sol->GetRoutes()[day][secondVhc];
            double curCost = 0.0;
            size_t firstPos = 0;
            size_t secondPos = 0;
            if (BestSwap(firstRt, secondRt, firstPos, secondPos, curCost))
            {
                swapExist = true;
                if (curCost < bestCost)
                {
                    bestCost = curCost;
                    bestFirstVhc = firstVhc;
                    bestSecondVhc = secondVhc;
                    bestFirstPos = firstPos;
                    bestSecondPos = secondPos;
                }
            }
        }
    }
    if ((swapExist) && (bestCost < 0))
    {
        Swap(day, bestFirstVhc, bestSecondVhc, bestFirstPos, bestSecondPos);
        return true;
    }
    return false;
}

void InterSwap::Swap(int day, int firstVhc, int secondVhc, size_t firstPos, size_t secondPos)
{
    vertex firstVertex = sol->GetRoutes()[day][firstVhc][firstPos];
    vertex secondVertex = sol->GetRoutes()[day][secondVhc][secondPos];
    
    sol->DelVertex(day, firstVhc, firstPos);
    sol->DelVertex(day, secondVhc, secondPos);
    sol->AddVertex(day, firstVhc, secondVertex, firstPos);
    sol->AddVertex(day, secondVhc, firstVertex, secondPos);
}

bool InterSwap::BestSwap(const route& firstRt, const route& secondRt, size_t& firstPos, size_t& secondPos, double& cost)
{
    bool result = false;
    double bestCost = std::numeric_limits<double>::max();
    for (size_t fPos = 0; fPos < firstRt.size(); ++fPos)
    {
        for (size_t sPos = 0; sPos < secondRt.size(); ++sPos)
        {
            double curCost = 0.0;
            if (CanSwap(firstRt, secondRt, fPos, sPos, curCost))
            {
                if (curCost < bestCost)
                {
                    firstPos = fPos;
                    secondPos = sPos;
                    bestCost = curCost;
                    cost = bestCost;
                    result = true;
                }
            }
        }
    }
    return result;
}
