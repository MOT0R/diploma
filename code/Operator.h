#ifndef OPERATOR_H_SENTRY
#define OPERATOR_H_SENTRY
#include <iostream>

class Operator
{
    size_t totalNumberOfCalls;
    size_t nbCallsSinceLastEval;
    double score;
    double weight;
protected:
    bool noise;
public:
    virtual ~Operator() {}
    Operator()
    {
        init();
    }
    void init()
    {
        totalNumberOfCalls = 0;
        nbCallsSinceLastEval = 0;
        score = 0;
        weight = 1;
    }

    void ResetNumberOfCalls()
    {
        nbCallsSinceLastEval = 0;
    }

    size_t GetTotalNumberOfCalls() { return totalNumberOfCalls; }
    size_t GetNumberOfCallsSinceLastEvaluation() { return nbCallsSinceLastEval; }

    void IncreaseNumberOfCalls()
    {
        totalNumberOfCalls++;
        nbCallsSinceLastEval++;
    }

    double GetScore() const { return score; }
    double GetWeight() const { return weight; } 
    void ResetScore() { this->score = 0; }
    void SetScore(double s) { this->score = s; }
    void SetWeight(double weight) { this->weight = weight; }
    void SetNoise() { noise = true; }
    void UnsetNoise() { noise = false; }
};

#endif
