#ifndef INTERREASSIGNMENT_H_SENTRY
#define INTERREASSIGNMENT_H_SENTRY

#include "Instance.h"
#include "NewSolution.h"

class InterReassignment
{
    Instance *inst;
    Solution *sol;

    double Length(const route& rt) const;
    bool BestReassignment(const route& firstRt, const route& secondRt, size_t& firstPos, size_t& secondPos, double& cost);
    void ApplyReassign(int day, int firstVhc, int secondVhc, size_t firstPos, size_t secondPos);
    bool ReassignPossible(const route& firstRt, const route& secondRt, size_t firstPos, size_t secondPos, double& cost);
    bool IsRouteFeasible(const route& rt) const;

public:
    InterReassignment(Instance& instance) : inst(&instance), sol(0) {}
    bool MakeBestNeighbor(Solution& sol, int day);
    ~InterReassignment() {}
};

#endif
