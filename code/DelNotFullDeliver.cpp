#include "DelNotFullDeliver.h"

void DelNotFullDeliver::DelRoute(int day, int vhc)
{
    route& rt = sol->GetRoutes()[day][vhc];
    while(!rt.size())
        sol->DelVertex(day, vhc, 0);
}

void DelNotFullDeliver::DestroySolution(Solution& solution)
{
    sol = &solution;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhcl = 0; vhcl < inst->numofvehicles; vhcl++)
        {
            if (!sol->GetRoutes()[day][vhcl].size())
                continue;
            if (sol->GetRoutes()[day][vhcl].Delivered() == inst->vehiclecap)
                continue;
            DelRoute(day, vhcl);
        }
    }
}
