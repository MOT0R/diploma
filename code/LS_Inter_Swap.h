#ifndef INTERSWAP_H_SENTRY
#define INTERSWAP_H_SENTRY

#include "LS_Operator.h"

class InterSwap
{
    Instance *inst;
    Solution *sol;

    bool CanSwap(const route& firstRt, const route& secondRt, size_t firstPos, size_t secondPos, double& cost) const;
    bool IsRouteFeasible(const route& rt) const;
    double Length(const route& rt) const;
    void Swap(int day, int firstVhc, int secondVhc, size_t firstPos, size_t secondPos);
    bool BestSwap(const route& firstRt, const route& secondRt, size_t& firstPos, size_t& secondPos, double& cost);
public:
    InterSwap(Instance& instance) : inst(&instance), sol(0) {}
    virtual bool MakeBestNeighbor(Solution& sol, int day);
    virtual ~InterSwap() {}
};

#endif
