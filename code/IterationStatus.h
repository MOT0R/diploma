#ifndef ITERATIONSTATUS_H_SENTRY
#define ITERATIONSTATUS_H_SENTRY

#include <iostream>

class IterationStatus
{
public:
    enum State
    {
        TRUE,
        FALSE,
        UNKNOWN
    };
    IterationStatus()
    {
        iterationId = 0;
        nbIterationWithoutImprovementSinceLastReload = 0;
        nbIterationWithoutTransition = 0;
        nbIterationWithoutImprovementCurrent = 0;
        nbIterationWithoutTransition = 0;
        acceptedAsCurrentSolution = UNKNOWN;
        alreadyKnownSolution = UNKNOWN;
        improveCurrentSolution = UNKNOWN;
        newBestSolution = UNKNOWN;
        alreadyDestroyed = UNKNOWN;
        alreadyRepaired = UNKNOWN;
    }

    void PartialReinit()
    {
        SetAcceptedAsCurrentSolution(UNKNOWN);
        SetAlreadyKnownSolution(UNKNOWN);
        SetImproveCurrentSolution(UNKNOWN);
        SetNewBestSolution(UNKNOWN);
        SetAlreadyDestroyed(UNKNOWN);
        SetAlreadyRepaired(UNKNOWN);
    }

    State GetAcceptedAsCurrentSolution() const
    {
        return acceptedAsCurrentSolution;
    }

    State GetAlreadyKnownSolution() const { return alreadyKnownSolution; }

    State GetImproveCurrentSolution() const { return improveCurrentSolution; }

    size_t GetIterationId() const { return iterationId; }

    size_t GetNbIterationWithoutImprovement() const
        { return nbIterationWithoutImprovement; }

    size_t GetNbIterationWithoutImprovementCurrent() const
        { return nbIterationWithoutImprovementCurrent; }

    size_t GetNbIterationWithoutTransition() const
        { return nbIterationWithoutTransition; }

    State GetNewBestSolution() const { return newBestSolution; }

    void SetAcceptedAsCurrentSolution(State acceptedAsCurrentSolution)
    {
        this->acceptedAsCurrentSolution = acceptedAsCurrentSolution;
    }

    void SetAlreadyKnownSolution(State alreadyKnownSolution)
        { this->alreadyKnownSolution = alreadyKnownSolution; }

    void SetImproveCurrentSolution(State improveCurrentSolution)
        { this->improveCurrentSolution = improveCurrentSolution; }

    void SetIterationId(size_t iterationId)
        { this->iterationId = iterationId; }

    void SetNbIterationWithoutImprovement(size_t nbIterationWithoutImprovement)
    { this->nbIterationWithoutImprovement = nbIterationWithoutImprovement; }

    void SetNbIterationWithoutImprovementCurrent(size_t nbIterationWithoutImprovementCurrent)
    { this->nbIterationWithoutImprovementCurrent = nbIterationWithoutImprovementCurrent; }

    void SetNbIterationWithoutTransition(size_t nbIterationWithoutTransition)
    { this->nbIterationWithoutTransition = nbIterationWithoutTransition; }

    void SetNewBestSolution(State newBestSolution)
    { this->newBestSolution = newBestSolution; }

    ~IterationStatus() {}

    size_t GetNbIterationWithoutImprovementSinceLastReload() const
    { return nbIterationWithoutImprovementSinceLastReload; }

    void SetNbIterationWithoutImprovementSinceLastReload(size_t nb)
    { nbIterationWithoutImprovementSinceLastReload = nb; }

    State GetAlreadyDestoyed() const { return alreadyDestroyed; }

    void SetAlreadyDestroyed(State alreadyDestroyed)
    { this->alreadyDestroyed = alreadyDestroyed; }

    State GetAlreadyRepaired() const { return alreadyRepaired; }

    void SetAlreadyRepaired(State alreadyRepaired)
    { this->alreadyRepaired = alreadyRepaired; }
    
private:
    size_t iterationId;
    size_t nbIterationWithoutImprovement;
    size_t nbIterationWithoutImprovementSinceLastReload;
    size_t nbIterationWithoutImprovementCurrent;
    size_t nbIterationWithoutTransition;
    State newBestSolution;
    State alreadyKnownSolution;
    State improveCurrentSolution;
    State alreadyRepaired;
    State alreadyDestroyed;
    State acceptedAsCurrentSolution;

};

#endif
