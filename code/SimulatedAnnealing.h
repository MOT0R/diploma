#ifndef SIMULATEDANNEALING_H_SENTRY
#define SIMULATEDANNEALING_H_SENTRY

#include "AcceptanceModule.h"
#include "NewSolution.h"
#include "CoolingSchedule.h"
#include "IterationStatus.h"
#include "BestSolutionManager.h"

class SimulatedAnnealing: public AcceptanceModule
{
    CoolingSchedule* coolingSchedule;
public:
    SimulatedAnnealing(CoolingSchedule& cs);

    virtual ~SimulatedAnnealing();

    bool TransitionAccepted(BestSolutionManager& bestSolManager,
            Solution& curSol,
            Solution& newSol,
            IterationStatus& status);

    virtual void PrintLog();

    virtual void startSignal();
};

#endif
