#ifndef DESTROYOPERATOR_H_SENTRY
#define DESTROYOPERATOR_H_SENTRY

#include "Operator.h"
#include "Instance.h"
#include "NewSolution.h"

class DestroyOperator : public Operator
{
public:
    DestroyOperator() {}
    virtual ~DestroyOperator() {}
    virtual void DestroySolution(Solution& sol) = 0; 
};

#endif
