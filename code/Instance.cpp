#include "Instance.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <fstream>

Instance::Instance(const Instance& instance) :
    numofplatforms(instance.numofplatforms), numofvehicles(instance.numofvehicles),
    vehiclecap(instance.vehiclecap), vehiclefuel(instance.vehiclefuel),
    numofdays(instance.numofdays), deliverydata(instance.deliverydata),
    platformcoordinates(instance.platformcoordinates)
{
    lengths = new double*[numofplatforms];
    for (int i = 0; i < numofplatforms; i++)
        lengths[i] = new double[numofplatforms];
    for (int i = 0; i < numofplatforms; i++)
        for (int j = 0; j < numofplatforms; j++)
            lengths[i][j] = instance.lengths[i][j];
}

void Instance::PrintAmounts() const
{
	std::cout << "numofvehicles = " << numofvehicles << '\n';
	std::cout << "numofplatforms = " << numofplatforms << '\n';
	std::cout << "vehiclecap = " << vehiclecap << '\n';
	std::cout << "vehiclefuel = " << vehiclefuel << '\n';
}

void Instance::PrintCoordinates() const // a lot of code doubling
{
	for (int i =0; i < 31; i++)
		std::cout << '-';
	std::cout << '\n';
	std::cout << std::setw(23) << "coordinates\n";
	for (int i =0; i < 31; i++)
		std::cout << '-';
	std::cout << '\n';
	for (int i = 0; i < numofplatforms; i++)
	{
		std::cout << '|';
        std::cout << std::fixed;  
        std::cout << std::setprecision(5);
        std::cout << std::setw(14) << platformcoordinates[i].x << '|';
        std::cout << std::setw(14) << platformcoordinates[i].y << '|';
		std::cout << '\n';
	}
	for (int i =0; i < 31; i++)
		std::cout << '-';
	std::cout << '\n';
}

void Instance::PrintInstance() const
{
}

void Instance::PrintTake() const
{
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	std::cout << std::setw(10) << "take\n";
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	for (int i = 0; i < numofdays; i++)
	{
		std::cout << '|';
		for (int j = 0; j < numofplatforms; j++)
		{
			std::cout << std::setw(4) << deliverydata[i][j].reqtake << '|';
		}
		std::cout << '\n';
	}
}

void Instance::PrintCanTake() const
{
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	std::cout << std::setw(10) << "cantake\n";
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	for (int i = 0; i < numofdays; i++)
	{
		std::cout << '|';
		for (int j = 0; j < numofplatforms; j++)
		{
			std::cout << std::setw(4) << deliverydata[i][j].cantake << '|';
		}
		std::cout << '\n';
	}
}

void Instance::PrintDeliver() const
{
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	std::cout << std::setw(13) << "deliver\n";
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	for (int i = 0; i < numofdays; i++)
	{
		std::cout << '|';
		for (int j = 0; j < numofplatforms; j++)
		{
			std::cout << std::setw(4) << deliverydata[i][j].reqdeliver << '|';
		}
		std::cout << '\n';
	}
}

// remove double code
void Instance::PrintCanDeliver() const
{
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	std::cout << std::setw(13) << "candeliver\n";
	for (int i =0; i < 5 * numofplatforms + 1; i++)
		std::cout << '-';
	std::cout << '\n';
	for (int i = 0; i < numofdays; i++)
	{
		std::cout << '|';
		for (int j = 0; j < numofplatforms; j++)
		{
			std::cout << std::setw(4) << deliverydata[i][j].candeliver << '|';
		}
		std::cout << '\n';
	}
}

void Instance::PrintLength() const
{
    for (int i = 0; i < numofplatforms; ++i)
    {
        for (int j = 0; j < numofplatforms; ++j)
        {
            std::cout << std::setw(10) << lengths[i][j] << " ";
        }
        std::cout << "\n";
    }
}

Instance::Instance(const char *filename) 
{
	std::ifstream f(filename);
	if (!f.is_open())
		std::cout << "File can't be opened\n";
	f >> numofplatforms;
	f >> numofvehicles;
	f >> vehiclecap;
	f >> vehiclefuel;
	f >> numofdays;
    deliverydata.assign(numofdays, std::vector<singledata>(numofplatforms));
    platformcoordinates.assign(numofplatforms, coordinates());
	for (int i = 0; i < numofplatforms; i++)
    {
			f >> platformcoordinates[i].x;
            f >> platformcoordinates[i].y;
    }
	for (int i = 0; i < numofdays; i++)
	{
		for (int j = 0; j < numofplatforms; j++)
			f >> deliverydata[i][j].reqtake;
		for (int j = 0; j < numofplatforms; j++)
			f >> deliverydata[i][j].cantake;
		for (int k = 0; k < numofplatforms; k++)
			f >> deliverydata[i][k].reqdeliver;
		for (int j = 0; j < numofplatforms; j++)
			f >> deliverydata[i][j].candeliver;
	}
	f.close();
    lengths = new double*[numofplatforms];
    for (int i = 0; i < numofplatforms; i++)
    {
        lengths[i] = new double[numofplatforms];
    }
	for (int i = 0; i < numofplatforms; i++)
    {
		for (int j = 0; j < numofplatforms; j++)
		{
			double diff[2];
			diff[0] = platformcoordinates[i].x - platformcoordinates[j].x;
			diff[1] = platformcoordinates[i].y - platformcoordinates[j].y;
			lengths[i][j] = sqrt(diff[0]*diff[0] + diff[1]*diff[1]) / 1000;
		}
    }
}

Instance::~Instance()
{
    for (int i = 0; i < numofplatforms; i++)
        delete [] lengths[i];
    delete [] lengths;
}
