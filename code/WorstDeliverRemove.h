#ifndef WORSTDELIVERREMOVE_H_SENTRY
#define WORSTDELIVERREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class WorstDeliverRemove : public DestroyOperator
{
    Instance *inst;
    size_t WorstPosition(int day, int vehicle, Solution& sol);
    public:
    virtual void DestroySolution(Solution& sol);
    virtual ~WorstDeliverRemove() {}
    WorstDeliverRemove(Instance& i) : inst(&i) {}
};

#endif
