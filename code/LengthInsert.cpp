#include "LengthInsert.h"
#include <limits>
#include <cassert>

bool LengthInsert::IsUnInserted(int vtx)
{
    const std::vector<vertex>& vec = sol->GetUnInserted();
    for (size_t pos = 0; pos < vec.size(); ++pos)
    {
        if (vec[pos].vertexNum == vtx)
            return true;
    }
    return false;
}

void LengthInsert::EvalBestParam(int& day, int& vhc, size_t& pos, double& cost, int vtx)
{
    double bestCost = std::numeric_limits<double>::max();
    for (int d = 0; d < inst->numofdays; ++d)
    {
        int planTake = sol->MaxPlanTake(d, vtx);
        int planDeliver = sol->MaxPlanDeliver(d, vtx);
        if (planTake + planDeliver == 0)
            continue;
        for (int v = 0; v < inst->numofvehicles; ++v)
        {
            if (sol->IsInRoute(d, v, vtx))
                continue;
            size_t bestPos = sol->BestLengthPos(d, v, vtx);
            if (bestPos > sol->GetRoutes()[d][v].size())
                continue;
            int canTake = sol->MaxPossibleTake(d, v, bestPos);
            int canDeliver = sol->MaxPossibleDeliver(d, v, bestPos);
            canTake = std::min(canTake, planTake);
            canDeliver = std::min(canDeliver, planDeliver);
            if (canTake + canDeliver == 0)
                continue;
            if (!sol->CanInsert(d, v, vtx, bestPos))
                continue;
            if (sol->InsertCost(d, v, vtx, bestPos) < bestCost)
            {
                bestCost = sol->InsertCost(d, v, vtx, bestPos);
                cost = bestCost;
                pos = bestPos;
                day = d;
                vhc = v;
            }
        }
    }
}

struct LengthInsert::iBestParam
{
    int vtxNum;
    int day;
    int vhc;
    size_t pos;
    double insertCost;
};

void LengthInsert::RepairSolution(Solution& solution)
{
    sol = &solution;
    size_t size = sol->GetUnInserted().size();
    const std::vector<vertex>& unInserted = sol->GetUnInserted();
    for (size_t i = 0; i < size; ++i)
        InsertInExisted(unInserted[i].vertexNum);
    iBestParam costs[size];
    for (size_t i = 0; i < size; ++i)
    {
        costs[i].vtxNum = unInserted[i].vertexNum;
        EvalBestParam(costs[i].day,
                costs[i].vhc,
                costs[i].pos,
                costs[i].insertCost,
                costs[i].vtxNum);
    }
    while(true)
    {
        iBestParam best;
        best.insertCost = std::numeric_limits<double>::max();
        best.day = -1;
        best.vhc = -1;
        best.pos = 0;
        best.vtxNum = -1;
        for (size_t i = 0; i < size; ++i)
        {
            if (!IsUnInserted(costs[i].vtxNum))
                continue;
            if (costs[i].insertCost < best.insertCost)
            {
                best.insertCost = costs[i].insertCost;
                best.day = costs[i].day;
                best.vhc = costs[i].vhc;
                best.pos = costs[i].pos;
                best.vtxNum = costs[i].vtxNum;
            }
        }
        if (best.vtxNum == -1)
            break;
        int planTake = sol->MaxPlanTake(best.day, best.vtxNum);
        int planDeliver = sol->MaxPlanDeliver(best.day, best.vtxNum);
        int canTake = sol->MaxPossibleTake(best.day, best.vhc, best.pos);
        int canDeliver = sol->MaxPossibleDeliver(best.day, best.vhc, best.pos);
        assert(canTake + canDeliver > 0);
        canTake = std::min(canTake, planTake);
        canDeliver = std::min(canDeliver, planDeliver);
        sol->AddVertex(best.day, best.vhc, vertex(best.vtxNum, canTake, canDeliver), best.pos);
        for (size_t i = 0; i < size; ++i)
        {
                EvalBestParam(costs[i].day,
                        costs[i].vhc,
                        costs[i].pos,
                        costs[i].insertCost,
                        costs[i].vtxNum);
        }
    }
}

size_t LengthInsert::VertexPos(int day, int vehicle, int vtx)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        if (rt[pos].vertexNum == vtx)
            return pos;
    }
    return rt.size();
}

void LengthInsert::InsertInExisted(int vtx)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            if (!sol->IsInRoute(day, vhc, vtx))
                continue;
            size_t vtxPos = VertexPos(day, vhc, vtx);
            int constrTake = sol->MaxPossibleTake(day, vhc, vtxPos);
            int constrDeliver = sol->MaxPossibleDeliver(day, vhc, vtxPos);
            int planTake = sol->MaxPlanTake(day, vtx);
            int planDeliver = sol->MaxPlanDeliver(day, vtx);
            int take = std::min(constrTake, planTake);
            int deliver = std::min(constrDeliver, planDeliver);
            sol->GetRoutes()[day][vhc][vtxPos].take += take;
            sol->GetRoutes()[day][vhc][vtxPos].deliver += deliver;
            if (take + deliver == 0)
                continue;
            sol->DelUnInserted(vertex(vtx, take, deliver));
            sol->GetDone()[day][vtx].take += take;
            sol->GetDone()[day][vtx].deliver += deliver;
            sol->GetRoutes()[day][vhc].CountDelivered();
            sol->GetRoutes()[day][vhc].CountTaken();
        }
        if (!IsUnInserted(vtx))
            return;
    }
}
