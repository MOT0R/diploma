#ifndef COOLINGSCHEDULE_H_SENTRY
#define COOLINGSCHEDULE_H_SENTRY

class CoolingSchedule
{
public:
    virtual double GetCurrentTemperature()=0;
    virtual void startSignal(){};
    virtual ~CoolingSchedule() {}
    virtual void PrintLog(){};
};

#endif
