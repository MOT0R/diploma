#include <limits>
#include "NewInit.h"

Initializator::Initializator(Instance& in, Solution& s)
{
    inst = &in;
    sol = &s;
    inserted = new bool*[inst->numofdays];
    for (int i = 0; i < inst->numofdays; ++i)
        inserted[i] = new bool[inst->numofplatforms];
    for (int i = 0; i < inst->numofdays; ++i)
        for (int j = 0; j < inst->numofplatforms; ++j)
            inserted[i][j] = false;
}

Initializator::~Initializator()
{
    for (int i = 0; i < inst->numofdays; ++i)
        delete [] inserted[i];
    delete [] inserted;
}

int Initializator::ChooseNearest(int day, int vehicle, int vtx)
{
    double smallestLength = std::numeric_limits<double>::max();
    int platform = -1;
    for (int plt = 1; plt < inst->numofplatforms; plt++)
    {
        if ((!inserted[day][plt]) && (plt != vtx) && 
                !sol->IsInRoute(day, vehicle, plt))
        {
            double length = inst->lengths[vtx][plt];
            if (length < smallestLength)
            {
                smallestLength = length;
                platform = plt;
            }
        }
    }
    return platform;
}

int Initializator::CanTake(int day, int vtx)
{
    int done = sol->GetDone()[day][vtx].take;
    int initTake = inst->deliverydata[day][vtx].cantake;
    return initTake - done;
}

int Initializator::CanDeliver(int day, int vtx)
{
    int done = sol->GetDone()[day][vtx].deliver;
    int initDeliver = inst->deliverydata[day][vtx].candeliver;
    return initDeliver - done;
}

void Initializator::InitOneVehicle(int day, int vehicle)
{
    bool end = false;
    int curVertex = 0;
    int pos = 0;
    while(!end)
    {
        int newVertex = ChooseNearest(day, vehicle, curVertex);
        if (newVertex == -1)
            return;
        if (!sol->CanInsert(day, vehicle, newVertex, pos))
            return;
        int canTake = CanTake(day, newVertex);
        int canDeliver = CanDeliver(day, newVertex);
        if (canTake + canDeliver == 0)
        {
            inserted[day][newVertex] = true;
            continue;
        }
        int possibleTake = sol->MaxPossibleTake(day, vehicle, pos);
        int possibleDeliver = sol->MaxPossibleDeliver(day, vehicle, pos);
        int take = std::min(canTake, possibleTake);
        int deliver = std::min(canDeliver, possibleDeliver);
        if (take + deliver == 0)
            return;
        sol->AddVertex(day, vehicle, vertex(newVertex, take, deliver), pos);
        pos++;
        curVertex = newVertex;
    }
}

void Initializator::InitOneDay(int day)
{
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
        InitOneVehicle(day, vehicle);
}

void Initializator::Init()
{
    for (int day = 0; day < inst->numofdays; day++)
        InitOneDay(day);
}
