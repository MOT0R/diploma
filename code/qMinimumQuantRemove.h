#ifndef MINIMUMQUANTREMOVE_H_SENTRY
#define MINIMUMQUANTREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class qMinimumQRemove : public DestroyOperator
{
    Instance *inst;
    Solution *sol;

    void WorstPosDemand(int day, int vehicle, size_t& pos, int& demand);
    void RemoveWorstPos(int day);
    int NumOfVertices(int day);

public:
    virtual void DestroySolution(Solution& solution);
    virtual ~qMinimumQRemove() {}
    qMinimumQRemove(Instance& instance) : inst(&instance) {}
};

#endif
