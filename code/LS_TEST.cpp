#include "LS.h"
#include "LS_Intra_Swap.h"
#include "LS_Inter_Swap.h"
#include "NewSolution.h"
#include "Instance.h"
#include "NewInit.h"
#include <fstream>

using namespace std;

int main()
{
    ifstream ist("name.txt");
    if (!ist)
        cout << "UNABLE TO OPEN NAME FILE\n";
    string name;
    ist >> name;

    Instance inst(name.c_str());
    Solution sol(inst);

    Initializator In(inst, sol);
    In.Init();

    sol.Print();
    if (sol.IsFeasible())
        cout << "FEASIBLE\n";
    else
        cout << "NOT FEASIBLE\n";

    cout << "OBJ: " << sol.GetObjective() << '\n';

    LocalSearch LS(inst);
    LS.PerformLocalSearch(sol);

    sol.Print();
    if (sol.IsFeasible())
        cout << "FEASIBLE\n";
    else
        cout << "NOT FEASIBLE\n";

    cout << "OBJ: " << sol.GetObjective() << '\n';

}
