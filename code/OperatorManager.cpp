#include <iostream>
#include "OperatorManager.h"
#include "Parameters.h"
#include "Operator.h"
#include "RepairOperator.h"
#include "DestroyOperator.h"
#include "IterationStatus.h"

OperatorManager::OperatorManager(Parameters& param)
{
    parameters = &param;
    sumWeightsRepair = 0;
    sumWeightsDestroy = 0;
    noise = false;
    performanceRepairOperatorsWithNoise = 1;
    performanceRepairOperatorsWithoutNoise = 1;
}

OperatorManager::~OperatorManager()
{
}

void OperatorManager::RecomputeWeight(Operator& op, double& sumW)
{
    double prevWeight = op.GetWeight();
    sumW -= prevWeight;
    double currentScore = op.GetScore();
    size_t nbCalls = op.GetNumberOfCallsSinceLastEvaluation();
    double newWeight = (1 - parameters->GetRho())*prevWeight +
        parameters->GetRho()*(static_cast<double>(nbCalls)/static_cast<double>(parameters->GetTimeSegmetsIt()))*currentScore;
    if(newWeight > parameters->GetMaximumWeight())
        newWeight = parameters->GetMaximumWeight();
    if(newWeight < parameters->GetMinimumWeight())
        newWeight = parameters->GetMinimumWeight();
    sumW += newWeight;
    op.SetWeight(newWeight);
    op.ResetScore();
    op.ResetNumberOfCalls();
}

void OperatorManager::RecomputeWeights()
{
    std::vector<size_t>* nbCalls = new std::vector<size_t>();
    for (size_t i = 0; i < repairOperators.size(); i++)
        nbCalls->push_back(repairOperators[i]->GetNumberOfCallsSinceLastEvaluation());
    for (size_t i = 0; i < destroyOperators.size(); i++)
        nbCalls->push_back(destroyOperators[i]->GetNumberOfCallsSinceLastEvaluation());


    //WEIGHT RECOMPUTATION FOR REPAIR
    for (size_t i = 0; i < repairOperators.size(); i++)
        RecomputeWeight(dynamic_cast<Operator&>(*(repairOperators[i])), sumWeightsRepair);

    //WEIGT RECOMPUTATION FOR DESTROY
    for (size_t i = 0; i < destroyOperators.size(); i++)
        RecomputeWeight(dynamic_cast<Operator&>(*(destroyOperators[i])), sumWeightsDestroy);

}

Operator& OperatorManager::SelectOperator(std::vector<Operator*>& vecOp, double sumW)
{
    double randomVal = static_cast<double>(rand())/static_cast<double>(RAND_MAX);
    double randomWeightPos = randomVal * sumW;
    double cumulSum = 0;
    for(size_t i = 0; i < vecOp.size(); i++)
    {
        cumulSum += vecOp[i]->GetWeight();
        if (cumulSum >= randomWeightPos)
        {
            vecOp[i]->IncreaseNumberOfCalls();
            return *(vecOp[i]);
        }
    }
    assert(false);
    return *(vecOp.back());
}

DestroyOperator& OperatorManager::SelectDestroyOperator()
{
    return dynamic_cast<DestroyOperator&>(SelectOperator(destroyOperators, sumWeightsDestroy));
}

RepairOperator& OperatorManager::SelectRepairOperator()
{
    return dynamic_cast<RepairOperator&>(SelectOperator(repairOperators,sumWeightsRepair));
}

void OperatorManager::AddRepairOperator(RepairOperator& repairOperator)
{
    repairOperators.push_back(&repairOperator);
    sumWeightsRepair += repairOperator.GetWeight();
}

void OperatorManager::AddDestroyOperator(DestroyOperator& destroyOperator)
{
    destroyOperators.push_back(&destroyOperator);
    sumWeightsDestroy += destroyOperator.GetWeight();
}

void OperatorManager::SanityCheck()
{
    assert(!repairOperators.empty());
    assert(!destroyOperators.empty());
    assert(sumWeightsDestroy > 0);
    assert(sumWeightsRepair > 0);
}

void OperatorManager::UpdateScores(DestroyOperator& des, RepairOperator& rep, IterationStatus& status)
{
    if (status.GetNewBestSolution() == IterationStatus::TRUE)
    {
        rep.SetScore(rep.GetScore() + parameters->GetSigma1());
        des.SetScore(des.GetScore() + parameters->GetSigma1());
    }

    if (status.GetImproveCurrentSolution() == IterationStatus::TRUE)
    {
        rep.SetScore(rep.GetScore() + parameters->GetSigma2());
        des.SetScore(des.GetScore() + parameters->GetSigma2());
    }

    if (status.GetImproveCurrentSolution() == IterationStatus::FALSE
            && status.GetAcceptedAsCurrentSolution() == IterationStatus::TRUE
            && status.GetAlreadyKnownSolution() == IterationStatus::FALSE)
    {
        rep.SetScore(rep.GetScore() + parameters->GetSigma3());
        des.SetScore(des.GetScore() + parameters->GetSigma3());
    }
}

void OperatorManager::end()
{
    for (int i = 0; i < repairOperators.size(); ++i)
        delete repairOperators[i];
    for (int i = 0; i < destroyOperators.size(); ++i)
        delete destroyOperators[i];
}


