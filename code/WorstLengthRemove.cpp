#include <limits>
#include <cassert>
#include "WorstLengthRemove.h"

size_t WorstLengthRemove::WorstPosition(int day, int vehicle, Solution& sol)
{
    route& rt = sol.GetRoutes()[day][vehicle];
    assert(rt.size() > 0);
    double worst = 0;
    size_t worstPos = 0;
    for (size_t pos = 0; pos < rt.size(); ++pos)
    {
        double cost = sol.DeleteCost(day, vehicle, pos);
        if (cost > worst)
        {
            worst = cost;
            worstPos = pos;
        }
    }
    return worstPos;
}

void WorstLengthRemove::DestroySolution(Solution& sol)
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhcl = 0; vhcl < inst->numofvehicles; vhcl++)
        {
            if (!sol.GetRoutes()[day][vhcl].size())
                continue;
            int pos = WorstPosition(day, vhcl, sol);
            sol.DelVertex(day, vhcl, pos);
        }
    }
}
