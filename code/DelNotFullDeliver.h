#ifndef DELNOTTFULLDELIVER_H_SENTRY
#define DELNOTTFULLDELIVER_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class DelNotFullDeliver : public DestroyOperator
{
    Solution *sol;
    Instance *inst;
    void DelRoute(int day, int vhc);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~DelNotFullDeliver() {}
    DelNotFullDeliver(Instance& i) : sol(0), inst(&i) {}
};


#endif
