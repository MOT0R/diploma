#ifndef SIMPLEACCEPTANCEMODULE_H_SENTRY
#define SIMPLEACCEPTANCEMODULE_H_SENTRY

#include "AcceptanceModule.h"

class Solution;
class IterationStatus;
class BestSolutionManager;

/*!
 * \class DummyAcceptanceModule.
 * \brief This module accept any solution as the current solution.
 * \details It can be used when the diversification is ensured through an other mechanism.
 * For example when the operators embbed some tabu criterion to prevent them from rebuilding
 * the previous solution.
 */

class SimpleAcceptanceModule: public AcceptanceModule {
public:
	//! Constructor.
	SimpleAcceptanceModule();
	//! Destructor.
	virtual ~SimpleAcceptanceModule();
	//! Compute if the newly created solution have to be accepted or not
	virtual bool TransitionAccepted(BestSolutionManager& bestSolutionManager,
							Solution& currentSolution,
							Solution& newSolution,
							IterationStatus& status);
    virtual void PrintLog() {}

};

#endif
