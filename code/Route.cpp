#include <cassert>
#include "Route.h"

route::route() 
{
    takenFromDepot = 0;
    deliveredToDepot = 0;
}

void route::AddVertex(const vertex& vtx, size_t pos)
{
    if (pos < vec.size())
        vec.insert(vec.begin() + pos, vtx);
    else
        vec.push_back(vtx);
}

void route::DelVertex(size_t pos)
{
    if (pos < vec.size())
        vec.erase(vec.begin() + pos);
}

void route::CountTaken()
{
    int taken = 0;
    for (size_t i = 0; i < vec.size(); ++i)
        taken += vec[i].deliver;
    takenFromDepot = taken;
}

void route::CountDelivered()
{
    int delivered = 0;
    for (size_t i = 0; i < vec.size(); ++i)
        delivered += vec[i].take;
    deliveredToDepot = delivered;
}

int route::OnBoard(size_t pos) const
{
    assert(pos < vec.size());
    int onBoard = takenFromDepot;
    for (size_t i = 0; i <= pos; ++i)
    {
        onBoard += vec[i].take;
        onBoard -= vec[i].deliver;
    }
    return onBoard;
}

const route& route::operator=(const route& rt)
{
    takenFromDepot = rt.takenFromDepot;
    deliveredToDepot = rt.deliveredToDepot;
    vec = rt.vec;
    return *this;
}

void route::Print() const
{
    if (!vec.size())
        return;
    std::cout << "0 (" << takenFromDepot << ", 0) -> ";
    for (size_t i = 0; i < vec.size(); i++) 
    {
        std::cout << vec[i].vertexNum << " (" << vec[i].take;
        std::cout << ", " << vec[i].deliver << ") -> ";
    }
    std::cout << "0 (0, " << deliveredToDepot << ")\n";
}
