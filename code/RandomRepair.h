#ifndef RANDOMREPAIR_H_SENTRY
#define RANDOMREPAIR_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include "NewSolution.h"

class RandomRepair : public RepairOperator
{
    Solution *sol;
    Instance *inst;

    size_t RandomPosition(int day, int vehicle, int vtx);
    int RandomVehicle(int day, int vtx);
    int RandomDay(int vtx);
    bool PossibleToInsert(int day, int vehicle, int vtx);
public:
    RandomRepair(Instance& i) : sol(0), inst(&i) {}
    virtual ~RandomRepair() {}
    virtual void RepairSolution(Solution& solution);
};

#endif
