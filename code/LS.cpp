#include "LS.h"
#include "LS_Inter_Swap.h"
#include "LS_Intra_Swap.h"
#include "LS_Inter_Reassignment.h"

bool LocalSearch::PerformLocalSearch(Solution &solution)
{
    bool result = false;
    sol = &solution;
    while (PerformOneIteration()) 
    {
        result = true;
    }
    return result;
}

bool LocalSearch::PerformOneIteration()
{
    double bestCost = 0.0;
    operators bestOperators[inst->numofdays];
    for (int day = 0; day < inst->numofdays; ++day)
        bestOperators[day] = NONE;

    IntraSwap IntraSwapOperator(*inst);
    InterSwap InterSwapOperator(*inst);
    InterReassignment InterReassignment(*inst);

    Solution testSol(*sol); 

    for (int day = 0; day < inst->numofdays; day++)
    {
        testSol = *sol;

        if (InterSwapOperator.MakeBestNeighbor(testSol, day))
        {
            double newCost = testSol.GetObjective() - sol->GetObjective();
            if (newCost < bestCost)
            {
                bestCost = newCost;
                bestOperators[day] = INTERSWAP;
            }
        }
        
        testSol = *sol;

        if (IntraSwapOperator.MakeBestNeighbor(testSol, day))
        {
            double newCost = testSol.GetObjective() - sol->GetObjective();
            if (newCost < bestCost)
            {
                bestCost = newCost;
                bestOperators[day] = INTRASWAP;
            }
        }

        testSol = *sol;

        if (InterReassignment.MakeBestNeighbor(testSol, day))
        {
            double newCost = testSol.GetObjective() - sol->GetObjective();
            if (newCost < bestCost)
            {
                bestCost = newCost;
                bestOperators[day] = INTERREASSIGNMENT;
            }
        }
    }
    
    bool result = false;

    for (int day = 0; day < inst->numofdays; ++day)
    {
        switch (bestOperators[day]) 
        {
            case NONE : break;

            case INTERSWAP : InterSwapOperator.MakeBestNeighbor(*sol, day);
                             result = true;
                             break;

            case INTRASWAP : IntraSwapOperator.MakeBestNeighbor(*sol, day);
                             result = true;
                             break;
            case INTERREASSIGNMENT : InterReassignment.MakeBestNeighbor(*sol, day);
                                     result = true;
                                     break;
        }
    }

    if (result)
        std::cout << "BOOOBA\n";
    return result;
}
