#include "Parameters.h"
#include <iostream>

Parameters::Parameters()
{
    maxNbIterations = 40000;//40000;
    maxRunningTime = 10;
    stopCrit = MAX_IT;
    noise = false;
    timeSegmentsIt = 100;
    nbItBeforeReinit = 1000;
    sigma1 = 33;
    sigma2 = 20;
    sigma3 = 15;
    rho = 0.1;
    minimumWeight = 1; //0.1;
    maximumWeight = 5;
    reloadFrequency = 10000;
    logFrequency = 100;
    lock = false;
    performLocalSearch = true;
}

Parameters::~Parameters() {}

Parameters::Parameters(Parameters& p)
{
    maxNbIterations = p.maxNbIterations;
    maxRunningTime = p.maxRunningTime;
    maxNbIterationsNoImp = p.maxNbIterationsNoImp;
    stopCrit = p.stopCrit;
    noise = p.noise;
    timeSegmentsIt = p.timeSegmentsIt;
    nbItBeforeReinit = p.nbItBeforeReinit;
    sigma1 = p.sigma1;
    sigma2 = p.sigma2;
    sigma3 = p.sigma3;
    rho = p.rho;
    minimumWeight = p.minimumWeight;
    maximumWeight = p.maximumWeight;
    reloadFrequency = p.reloadFrequency;
    performLocalSearch = p.performLocalSearch;
}
