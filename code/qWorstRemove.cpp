#include "qWorstRemove.h"

int qWorstRemove::NumOfVertices(int day)
{
    int num = 0;
    for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
    {
        num += sol->GetRoutes()[day][vehicle].size();
    }
    return num;
}

void qWorstRemove::DestroySolution(Solution& solution)
{
    sol = &solution;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        double num = NumOfVertices(day);
        double q = num / 100 * 20;
        int numToRemove = q;
        while(numToRemove != 0)
        {
            RemoveWorstPos();
            numToRemove--;
        }
    }
}

void qWorstRemove::WorstPosCost(int day, int vehicle, size_t& pos, double& cost)
{
    route& rt = sol->GetRoutes()[day][vehicle];
    cost = 0;
    pos = 0;
    for (size_t curPos = 0; curPos < rt.size(); ++curPos)
    {
        double curCost = sol->DeleteCost(day, vehicle, curPos);
        if (curCost > cost)
        {
            cost = curCost;
            pos = curPos;
        }
    }
}

void qWorstRemove::RemoveWorstPos()
{
    int worstDay = 0;
    int worstVehicle = 0;
    size_t worstPos = 0;
    double worstCost = 0;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vehicle = 0; vehicle < inst->numofvehicles; ++vehicle)
        {
            route& rt = sol->GetRoutes()[day][vehicle];
            if (rt.size() <= 1)
                continue;
            size_t pos = 0;
            double cost = 0;
            WorstPosCost(day, vehicle, pos, cost);
            if (cost > worstCost)
            {
                worstCost = cost;
                worstPos = pos;
                worstVehicle = vehicle;
                worstDay = day;
            }
        }
    }
    sol->DelVertex(worstDay, worstVehicle, worstPos);
}
