#ifndef LSOPERATOR_H_SENTRY
#define LSOPERATOR_H_SENTRY

#include "NewSolution.h"
#include "Instance.h"

class LSOperator
{
public:
    LSOperator() {}
    virtual void MakeBestNeighbor(Solution& sol, int day) = 0;
    virtual ~LSOperator() {}
};

#endif
