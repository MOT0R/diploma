#include "NewSolution.h"
#include <cassert>
#include <limits>

Solution::Solution(Instance& in)
{
    inst = &in;
    obj = 0.0;
    done = new info*[in.numofdays];
    routes = new route*[in.numofdays];
    routeLength = new double*[in.numofdays];
    for (int i = 0; i < in.numofdays; ++i)
    {
        done[i] = new info[in.numofplatforms];
        routes[i] = new route[in.numofvehicles];
        routeLength[i] = new double[in.numofvehicles];
    }
    InitUnInserted();
}

Solution::~Solution()
{
    for (int i = 0; i < inst->numofdays; ++i)
    {
        delete [] done[i];
        delete [] routes[i];
        delete [] routeLength[i];
    }
    delete [] done;
    delete [] routes;
    delete [] routeLength;
}

void Solution::InitUnInserted()
{
    for (int plt = 1; plt < inst->numofplatforms; ++plt)
    {
        unInserted.push_back(vertex(plt, 0, 0));
        for (int day = 0; day < inst->numofdays; ++day)
        {
            int deliver = inst->deliverydata[day][plt].candeliver;
            int take = inst->deliverydata[day][plt].cantake;
            unInserted[plt - 1].deliver += deliver;
            unInserted[plt - 1].take += take;
        }
    }
}

Solution::Solution(const Solution& s)
{
    inst = s.inst;
    obj = s.obj;
    routes = new route*[inst->numofdays];
    routeLength = new double*[inst->numofdays];
    done = new info*[inst->numofdays];
    for (int i = 0; i < inst->numofdays; ++i)
    {
        done[i] = new info[inst->numofplatforms];
        routes[i] = new route[inst->numofvehicles];
        routeLength[i] = new double[inst->numofvehicles];
    }

    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int plt = 0; plt < inst->numofplatforms; ++plt)
        {
            done[day][plt] = s.done[day][plt];
        }
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            routes[day][vhc] = s.routes[day][vhc];
            routeLength[day][vhc] = s.routeLength[day][vhc];
        }
    }
    unInserted = s.unInserted;
}

const Solution& Solution::operator=(const Solution& s)
{
    inst = s.inst;
    obj = s.obj;

    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int plt = 0; plt < inst->numofplatforms; ++plt)
        {
            done[day][plt] = s.done[day][plt];
        }
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            routes[day][vhc] = s.routes[day][vhc];
            routeLength[day][vhc] = s.routeLength[day][vhc];
        }
    }

    unInserted = s.unInserted;
    return *this;
}

bool Solution::IsLengthFeasible()
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vcl = 0; vcl < inst->numofvehicles; ++vcl)
        {
            if (routeLength[day][vcl] > inst->vehiclefuel)
            {
                std::cout << "LENGTH VIOLATION ";
                std::cout << "DAY " << day;
                std::cout << " VEHICLE " << vcl;
                return false;
            }
        }
    }
    return true;
}

bool Solution::IsCapFeasible()
{
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vcl = 0; vcl < inst->numofvehicles; ++vcl)
        {
            route& rt = routes[day][vcl];
            int onBoard = rt.Taken();
            if (onBoard > inst->vehiclecap)
                return false;
            for (size_t pos = 0; pos < rt.size(); ++pos)
            {
                onBoard = rt.OnBoard(pos);
                if (onBoard > inst->vehiclecap)
                    return false;
            }
        }
    }
    return true;
}

bool Solution::IsDemandComplete()
{
    for (int platform = 1; platform < inst->numofplatforms; platform++)
    {
        int take = 0;
        int deliver = 0;
        int reqtake = 0;
        int reqdeliver = 0;
        int cantake = 0;
        int candeliver = 0;
        for (int day = 0; day < inst->numofdays; day++)
        {
            take += done[day][platform].take;
            deliver += done[day][platform].deliver;
            reqtake += inst->deliverydata[day][platform].reqtake;
            reqdeliver += inst->deliverydata[day][platform].reqdeliver;
            cantake += inst->deliverydata[day][platform].cantake;
            candeliver += inst->deliverydata[day][platform].candeliver;
            if (take < reqtake)
            {
                std::cout << "take < reqtake\n";
                return false;
            }
            if (take > cantake)
            {
                std::cout << "take > cantake\n";
                return false;
            }
            if (deliver < reqdeliver)
            {
                std::cout << "delvier < reqdeliver\n";
                return false;
            }
            if (deliver > candeliver)
            {
                std::cout << "deliver > candeliver\n";
                return false;
            }
        }
    }
    return true;
}

bool Solution::IsFeasible()
{
    bool result = true;
    if (!IsLengthFeasible())
        result = false;
    if (!IsCapFeasible())
        result = false;
    if (!IsDemandComplete())
        result = false;
    return result;
}

void Solution::RecomputeLength()
{
    for (int day = 0; day < inst->numofdays; day++)
        for (int vhc = 0; vhc < inst->numofvehicles; vhc++)
            routeLength[day][vhc] = EvalRouteLength(day, vhc);
}

void Solution::RecomputeObj()
{
    double result = 0.0;
    for (int day = 0; day < inst->numofdays; day++)
        for (int vhc = 0; vhc < inst->numofvehicles; vhc++)
            result += routeLength[day][vhc];
    obj = result;
}

double Solution::EvalRouteLength(int day, int vhc)
{
    route& rt = routes[day][vhc];
    if (!rt.size())
        return 0.0;
    double result = 0.0;
    result += inst->lengths[0][rt[0].vertexNum];
    for (size_t pos = 0; pos < rt.size() - 1; pos++)
    {
        result += inst->lengths[rt[pos].vertexNum][rt[pos + 1].vertexNum];
    }
    result += inst->lengths[0][rt[rt.size() - 1].vertexNum];
    return result;
}

double Solution::InsertCost(int day, int vehicle, int vtx, size_t pos)
{
    route& rt = routes[day][vehicle];
    if (!rt.size())
        return (2 * inst->lengths[0][vtx]);
    int prevvtx = 0;
    int nextvtx = 0;
    if (pos < rt.size())
        nextvtx = rt[pos].vertexNum;
    if (pos > 0)
        prevvtx = rt[pos - 1].vertexNum;
    double delta = 0.0;
    delta += inst->lengths[prevvtx][vtx];
    delta += inst->lengths[vtx][nextvtx];
    delta -= inst->lengths[prevvtx][nextvtx];
    return delta;
}

double Solution::DeleteCost(int day, int vehicle, size_t pos)
{
    route& rt = routes[day][vehicle];
    if(!rt.size())
        return 0.0;
    assert(pos < rt.size());
    int prevvtx = 0;
    int nextvtx = 0;
    if (pos < rt.size() - 1)
        nextvtx = rt[pos + 1].vertexNum;
    if (pos > 0)
        prevvtx = rt[pos - 1].vertexNum;
    double delta = 0.0;
    delta += inst->lengths[prevvtx][rt[pos].vertexNum];
    delta += inst->lengths[rt[pos].vertexNum][nextvtx];
    delta -= inst->lengths[prevvtx][nextvtx];
    return delta;
}

void Solution::AddVertex(int day, int vehicle, vertex vtx, size_t pos)
{
    route& rt = routes[day][vehicle];
    double delta = InsertCost(day, vehicle, vtx.vertexNum, pos);
    rt.AddVertex(vtx, pos);
    DelUnInserted(vtx);
    done[day][vtx.vertexNum].deliver += vtx.deliver;
    done[day][vtx.vertexNum].take += vtx.take;
    obj += delta;
    routeLength[day][vehicle] += delta;
    rt.CountDelivered();
    rt.CountTaken();
}

void Solution::DelVertex(int day, int vehicle, size_t pos)
{
    route& rt = routes[day][vehicle];
    double delta = DeleteCost(day, vehicle, pos);
    done[day][rt[pos].vertexNum].deliver -= rt[pos].deliver;
    done[day][rt[pos].vertexNum].take -= rt[pos].take;
    AddUnInserted(rt[pos]);
    rt.DelVertex(pos);
    obj -= delta;
    routeLength[day][vehicle] -= delta;
    rt.CountDelivered();
    rt.CountTaken();
}

int Solution::MaxPossibleDeliver(int day, int vehicle, size_t pos)
{
    route& rt = routes[day][vehicle];
    assert(pos <= rt.size());
    int maxLoad = rt.Taken();
    for (size_t i = 0; i < pos; ++i)
    {
        if(rt.OnBoard(i) > maxLoad)
            maxLoad = rt.OnBoard(i);
    }
    return inst->vehiclecap - maxLoad;
}

int Solution::MaxPossibleTake(int day, int vehicle, size_t pos)
{
    route& rt = routes[day][vehicle];
    assert(pos <= rt.size());
    int maxLoad = rt.Delivered();
    int curLoad = rt.Taken();
    for (size_t i = 0; i < pos; i++)
        curLoad = rt.OnBoard(i);
    if (curLoad > maxLoad)
        maxLoad = curLoad;
    for (size_t i = pos; i < rt.size(); i++)
    {
        if (rt.OnBoard(i) > maxLoad)
            maxLoad = rt.OnBoard(i);
    }
    return inst->vehiclecap - maxLoad;
}

int Solution::MaxPlanTake(int day, int vtx)
{
    int bufTake = 0;
    int reqtake = 0;
    int donetake = 0;
    int moretake = 0;
    int buf = 0;
    for (int i = 0; i < day; ++i)
    {
        bufTake += inst->deliverydata[i][vtx].cantake;
        bufTake -= inst->deliverydata[i][vtx].reqtake;
        donetake = done[i][vtx].take;
        reqtake = inst->deliverydata[i][vtx].reqtake;
        buf = donetake - reqtake;
        moretake += buf;
        if (moretake < 0)
            moretake = 0;
    }
    bufTake -= moretake;
    bufTake += inst->deliverydata[day][vtx].cantake;
    bufTake -= done[day][vtx].take;
    assert(bufTake >= 0);

    int cantake = 0;
    donetake = 0;
    int owetake = 0;
    
    for (int i = inst->numofdays - 1; i > day; --i)
    {
        donetake = done[i][vtx].take;
        cantake = inst->deliverydata[i][vtx].cantake;
        buf = donetake - cantake;
        owetake += buf;
        if (owetake < 0)
            owetake = 0;
    }
    bufTake -= owetake;
    assert(bufTake >= 0);
    return bufTake;
}

int Solution::MaxPlanDeliver(int day, int vtx)
{
    int bufDeliver = 0;
    int reqdeliver = 0;
    int donedeliver = 0;
    int moredeliver = 0;
    int buf = 0;
    for (int i = 0; i < day; ++i)
    {
        bufDeliver += inst->deliverydata[i][vtx].candeliver;
        bufDeliver -= inst->deliverydata[i][vtx].reqdeliver;
        donedeliver = done[i][vtx].deliver;
        reqdeliver = inst->deliverydata[i][vtx].reqdeliver;
        buf = donedeliver - reqdeliver;
        moredeliver += buf;
        if (moredeliver < 0)
            moredeliver = 0;
    }
    bufDeliver -= moredeliver;
    bufDeliver += inst->deliverydata[day][vtx].candeliver;
    bufDeliver -= done[day][vtx].deliver;
    assert(bufDeliver >= 0);

    int candeliver = 0;
    donedeliver = 0;
    int owedeliver = 0;
    
    for (int i = inst->numofdays - 1; i > day; --i)
    {
        donedeliver = done[i][vtx].deliver;
        candeliver = inst->deliverydata[i][vtx].candeliver;
        buf = donedeliver - candeliver;
        owedeliver += buf;
        if (owedeliver < 0)
            owedeliver = 0;
    }
    bufDeliver -= owedeliver;
    assert(bufDeliver >= 0);
    return bufDeliver;
}

size_t Solution::BestLengthPos(int day, int vehicle, int vtx)
{
    route& rt = routes[day][vehicle];
    double cost = std::numeric_limits<double>::max();
    size_t bestPos = rt.size() + 1;
    for (size_t pos = 0; pos <= rt.size(); pos++)
    {
        double curCost = InsertCost(day, vehicle, vtx, pos);
        if (curCost < cost)
        {
            int take = MaxPossibleTake(day, vehicle, pos);
            int deliver = MaxPossibleDeliver(day, vehicle, pos);
            take = std::min(take, MaxPlanTake(day, vtx));
            deliver = std::min(deliver, MaxPlanDeliver(day, vtx));
            if (take + deliver == 0)
                continue;
            cost = curCost;
            bestPos = pos;
        }
    }
    return bestPos;
}

size_t Solution::BestDemandPos(int day, int vehicle, int& take, int& deliver, int vtx)
{
    route& rt = routes[day][vehicle];
    int bestAmount = 0;
    size_t bestPos = rt.size() + 1;
    for (size_t pos = 0; pos <= rt.size(); ++pos)
    {
        int planTake = MaxPlanTake(day, vtx);
        int planDeliver = MaxPlanDeliver(day, vtx);
        int canTake = MaxPossibleTake(day, vehicle, pos);
        int canDeliver = MaxPossibleDeliver(day, vehicle, pos);
        canTake = std::min(canTake, planTake);
        canDeliver = std::min(canDeliver, planDeliver);
        if (canTake + canDeliver > bestAmount)
        {
            bestAmount = canTake + canDeliver;
            take = canTake;
            deliver = canDeliver;
            bestPos = pos;
        }
    }
    return bestPos;
}

bool Solution::IsInRoute(int day, int vehicle, int vtx)
{
    for (size_t i = 0; i < routes[day][vehicle].size(); ++i)
    {
        if (vtx == routes[day][vehicle][i].vertexNum)
            return true;
    }
    return false;
}

void Solution::AddUnInserted(vertex v)
{
    assert(v.deliver + v.take > 0);
    for (size_t i = 0; i < unInserted.size(); ++i)
    {
        if (v.vertexNum == unInserted[i].vertexNum)
        {
            unInserted[i].deliver += v.deliver;
            unInserted[i].take += v.take;
            return;
        }
    }
    unInserted.push_back(v);
}

void Solution::DelUnInserted(vertex v)
{
    for (size_t i = 0; i < unInserted.size(); ++i)
    {
        if (v.vertexNum == unInserted[i].vertexNum)
        {
            unInserted[i].deliver -= v.deliver;
            unInserted[i].take -= v.take;
            assert(unInserted[i].deliver >= 0);
            assert(unInserted[i].take >= 0);
            if (unInserted[i].deliver + unInserted[i].take == 0)
            {
                unInserted.erase(unInserted.begin() + i);
            }
            return;
        }
    }
    std::cout << "VERTEX " << v.vertexNum << "TAKE " << v.take << "DELIVER " << v.deliver << '\n';
    assert(false);
}

bool Solution::CanInsert(int day, int vehicle, int vtx, size_t pos)
{
    double length = routeLength[day][vehicle];
    double cost = InsertCost(day, vehicle, vtx, pos);
    if (length + cost < inst->vehiclefuel)
        return true;
    return false;
}

Solution* Solution::Copy()
{
    Solution* newSol = new Solution(*inst);
    for (int i = 0; i < inst->numofdays; ++i)
        for (int j = 0; j < inst->numofvehicles; ++j)
        {
            newSol->routes[i][j] = routes[i][j];
            newSol->routeLength[i][j] = routeLength[i][j];
        }
    for (int day = 0; day < inst->numofdays; ++day)
        for (int vtx = 0; vtx < inst->numofplatforms; ++vtx)
        {
            newSol->done[day][vtx] = done[day][vtx];
        }
    newSol->unInserted = unInserted;
    newSol->obj = obj;
    return newSol;
}

void Solution::Print() const
{
    for (int day = 0; day < inst->numofdays; day++)
    {
        std::cout << "\nDAY " << day << ":\n";
        for (int vehicle = 0; vehicle < inst->numofvehicles; vehicle++)
        {
            if (!routes[day][vehicle].size())
                continue;
            std::cout << "vehicle " << vehicle << ": ";
            routes[day][vehicle].Print();
        }
    }
    std::cout << '\n';
}

bool Solution::operator<(Solution& sol)
{
    return (obj < sol.obj);
}

long long Solution::GetHash()
{
    double hash = 0;
    for (int day = 0; day < inst->numofdays; ++day)
    {
        for (int vhc = 0; vhc < inst->numofvehicles; ++vhc)
        {
            route& rt = routes[day][vhc];
            if (!rt.size())
                break;
            for (size_t pos = 0; pos < routes[day][vhc].size() - 1; ++pos)
                hash += inst->lengths[rt[pos].vertexNum][rt[pos + 1].vertexNum] * inst->lengths[rt[pos].vertexNum][rt[pos + 1].vertexNum];
        }
    }
    return static_cast<long long>(100*hash);
}
