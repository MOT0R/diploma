#ifndef IBESTLENGTHINSERT_H_SENTRY
#define IBESTLENGTHINSERT_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include "NewSolution.h"

class IBestLengthInsert : public RepairOperator
{
    Solution *sol;
    Instance *inst;

    void InsertInExisted(int vtx);
    bool AddExist(int day, int vtx);
    void EvalBestParam(int day, int& vhc, int& take, int& deliver, size_t& pos, int vtx);
    void InsertInBestPos(int vtx, int day);
    int RandomDay(int vtx);
public:
    IBestLengthInsert(Instance& i) : sol(0), inst(&i) {}
    virtual ~IBestLengthInsert() {}
    virtual void RepairSolution(Solution& solution);
};

#endif
