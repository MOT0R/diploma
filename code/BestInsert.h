#ifndef BESTINSERT_H_SENTRY
#define BESTINSERT_H_SENTRY

#include "RepairOperator.h"
#include "Instance.h"
#include <vector>

class BestInsert : public RepairOperator
{
    const Instance *instance;
    std::vector<bool> completedDays;
    
public:
    BestInsert(Instance& instance);
    virtual ~BestInsert();
    void BestDayVehPos(Solution& sol, int vertex, int& day, int& veh, int& pos);
    virtual void RepairSolution(Solution& sol);
    void Insert(int vertex, Solution& sol);
};

#endif
