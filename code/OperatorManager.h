#ifndef OPERATORMANAGER_H_SENTRY
#define OPERATORMANAGER_H_SENTRY

#include <vector>
#include "RepairOperator.h"
#include "DestroyOperator.h"
#include "Parameters.h"
#include "IterationStatus.h"
#include "Statistics.h"

class OperatorManager
{
    std::vector<Operator*> repairOperators;
    std::vector<Operator*> destroyOperators;
    double sumWeightsRepair;
    double sumWeightsDestroy;
    Parameters* parameters;
    bool noise;
    double performanceRepairOperatorsWithNoise;
    double performanceRepairOperatorsWithoutNoise;
    Operator& SelectOperator(std::vector<Operator*>& vecOp, double sumW);
    void RecomputeWeight(Operator& op, double& sumW);
    Statistics* stats;
public:
    OperatorManager(Parameters& param); 
    virtual ~OperatorManager();
    void RecomputeWeights();
    DestroyOperator& SelectDestroyOperator();
    RepairOperator& SelectRepairOperator();
    void AddRepairOperator(RepairOperator& repairOperator);
    void AddDestroyOperator(DestroyOperator& destroyOperator);
    virtual void UpdateScores(DestroyOperator& des, RepairOperator& rep, IterationStatus& status);
    virtual void StartSignal() {}
    void SetStatistics(Statistics* statistics) { stats = statistics; }
    void SanityCheck();
    void end();
};

#endif
