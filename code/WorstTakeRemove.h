#ifndef WORSTTAKEREMOVE_H_SENTRY
#define WORSTTAKEREMOVE_H_SENTRY

#include "DestroyOperator.h"
#include "Route.h"

class WorstTakeRemove : public DestroyOperator
{
    Instance *inst;
    size_t WorstPosition(int day, int vehicle, Solution& sol);
    public:
    virtual void DestroySolution(Solution& sol);
    virtual ~WorstTakeRemove() {}
    WorstTakeRemove(Instance& i) : inst(&i) {}
};

#endif
