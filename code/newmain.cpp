#include "NewSolution.h"
#include "Instance.h"
#include "NewInit.h"
#include "BulkInit.h"
#include "WorstLengthRemove.h"
#include "NewRandomRemove.h"
#include "RandomRemove.h"
#include "qRandomRemove.h"
#include "qWorstRemove.h"
#include "qMinimumQuantRemove.h"
#include "WorstDeliverRemove.h"
#include "WorstTakeRemove.h"
#include "DelNotFullDeliver.h"
#include "DelNotFullTake.h"
#include "BestLengthInsert.h"
#include "RandomRepair.h"
#include "LengthInsert.h"
#include "BestDemandInsert.h"
#include "ImpBestLengthInsert.h"
#include "SimpleBestSolutionManager.h"
//#include "SimpleAcceptanceModule.h"
#include "CoolingSchedule.h"
#include "CoolingScheduleParameters.h"
#include "CoolingScheduleFactory.h"
#include "SimulatedAnnealing.h"
#include "ALNS.h"

#include "LS.h"
#include <fstream>
#include <string>
#include <ctime>
#include <random>

using namespace std;

int main()
{
#if 1
    // Instance initialization
    srand(time(0));
    ifstream ist("name.txt");
    if (!ist)
        cout << "UNABLE TO OPEN NAME FILE\n";
    string name;
    ist >> name;

    Instance inst(name.c_str());
    Solution sol(inst);

    //Initializator In(inst, sol);
    BulkInit In(inst, sol);
    In.Init();

    Parameters param;
    
    CoolingScheduleParameters csParam(param);
    SimpleBestSolutionManager bestSolManager(param);
    CoolingSchedule* cs = CoolingScheduleFactory::MakeCoolingSchedule(dynamic_cast<Solution&>(sol), csParam);
    SimulatedAnnealing sa(*cs);

    LocalSearch LS(inst);
    LocalSearchManager lsManager(param, LS);


    WorstLengthRemove wLengthR(inst);
    WorstDeliverRemove wDeliverR(inst);
    WorstTakeRemove wTakeR(inst);
    OneRandomRemove orandomR(inst);
    RandomRemove randomR(inst);
    QRandomRemove qrandomR(inst);
    qMinimumQRemove qMinQR(inst);
    DelNotFullDeliver dNotFullD(inst);
    DelNotFullTake dNotFullT(inst);
    qWorstRemove qWorstR(inst);

    RandomRepair randomI(inst);
    BestLengthInsert bLengthI(inst);
    LengthInsert lengthI(inst);
    BestDemandInsert bDemandI(inst);
    IBestLengthInsert IbLengthI(inst);

    OperatorManager opMan(param);
    //opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(wLengthR));
    //opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(dNotFullD));
    //opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(dNotFullT));
    //opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(wDeliverR));
    //opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(wTakeR));
    opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(qrandomR));
    opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(qWorstR));
    opMan.AddDestroyOperator(dynamic_cast<DestroyOperator&>(qMinQR));

    //opMan.AddRepairOperator(dynamic_cast<RepairOperator&>(bLengthI));
    opMan.AddRepairOperator(dynamic_cast<RepairOperator&>(lengthI));
    opMan.AddRepairOperator(dynamic_cast<RepairOperator&>(IbLengthI));
    opMan.AddRepairOperator(dynamic_cast<RepairOperator&>(bDemandI));
    //opMan.AddRepairOperator(dynamic_cast<RepairOperator&>(randomI));

    ALNS alns(inst,
            sol,
            param,
            opMan,
            bestSolManager,
            sa,
            lsManager);

    alns.solve();

#else
    srand(time(0));
    ifstream ist("name.txt");
    if (!ist)
        cout << "UNABLE TO OPEN NAME FILE\n";
    string name;
    ist >> name;

    Instance inst(name.c_str());
    Solution sol(inst);

    Initializator In(inst, sol);
    In.Init();
    sol.Print();
    if (sol.IsFeasible())
        cout << "FEASIBLE\n";
    else
        cout << "NOT FEASIBLE\n";

    cout << "OBJ: " << sol.GetObjective() << '\n';
    qMinimumQRemove R(inst);
    IBestLengthInsert B(inst);
    LocalSearch LS(inst);

    for (int i = 0; i < 1; i++)
    {
        R.DestroySolution(sol);
        sol.Print();
        cout << "OBJ: " << sol.GetObjective() << '\n';
        for (int i = 0; i < inst.numofplatforms; ++i)
            std::cout << sol.BestLengthPos(1, i, 3) << '\n';
        for (size_t pos = 0; pos < sol.GetUnInserted().size(); ++pos)
        {
            const std::vector<vertex>& ui = sol.GetUnInserted();
            cout << "v = " << ui[pos].vertexNum;
            cout << " take = " << ui[pos].take;
            cout << " deliver = " << ui[pos].deliver;
            cout << '\n';
        }
        B.RepairSolution(sol);

        LS.PerformLocalSearch(sol);

        sol.Print();

        if (sol.IsFeasible())
            cout << "FEASIBLE\n";
        else
            cout << "NOT FEASIBLE\n";
        cout << "OBJ: " << sol.GetObjective() << '\n';
        if (!sol.GetUnInserted().size())
            cout << "UNINSERTED IS EMPTY\n";
        for (int day = 0; day < inst.numofdays; day++)
        {
            for (int i = 0; i < inst.numofplatforms; i++)
                cout << sol.GetDone()[day][i].take << " ";
            cout <<'\n';
        }
    }
#endif
}
