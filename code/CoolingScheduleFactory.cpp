#include "CoolingScheduleFactory.h"
#include "ExpCoolingSchedule.h"

CoolingSchedule* CoolingScheduleFactory::MakeCoolingSchedule(Solution& sol,
        CoolingScheduleParameters& param)
{
    if (param.kind == CoolingScheduleParameters::EXP_IT)
    {
        std::cout << "EXP_IT" << '\n';
        return dynamic_cast<CoolingSchedule*>(new ExpCoolingSchedule(sol, param));
    }
    else
    {
        std::cout << "SCHEDULE ERROR\n";
    }
}
