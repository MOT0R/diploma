#ifndef RANDOMREMOVE_H_SENTRY
#define RANDOMREMOVE_H_SENTRY

#include "DestroyOperator.h"

class OneRandomRemove : public DestroyOperator
{
    Solution *sol;
    Instance *inst;
    size_t PosInRoute(int day, int vhc, int vtx);
public:
    virtual void DestroySolution(Solution& sol);
    virtual ~OneRandomRemove() {}
    OneRandomRemove(Instance& i) : inst(&i) {}
};

#endif
